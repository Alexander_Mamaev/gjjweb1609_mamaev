package com.getjavajob.training.web1609.mamaeva.lesson00.task1.base;

public class RegularPolyhedron implements ClosedRegularFigure2D {
    private int angleNumber;
    private double edgeLength;

    public RegularPolyhedron(int angleNumber, double edgeLength) {
        this.angleNumber = angleNumber;
        this.edgeLength = edgeLength;
    }

    @Override
    public double baseArea() {
        return angleNumber / 4. * edgeLength * edgeLength / Math.tan(Math.PI / angleNumber);
    }

    @Override
    public double perimeter() {
        return angleNumber * edgeLength;
    }

    // return the radius of inscribed circle
    @Override
    public double getRadius() {
        return 2 * baseArea() / perimeter();
    }
}

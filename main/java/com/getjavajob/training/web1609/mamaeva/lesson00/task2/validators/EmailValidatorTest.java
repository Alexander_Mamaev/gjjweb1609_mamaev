package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmailValidatorTest {
    private static Validator validator;
    private static Product product;

    @Before
    public void setUp() {
        validator = new EmailValidator();
        product = new Product();
    }

    @Test
    public void validateTest() {
        product.setEmail("mail@mail.ru");
        assertEquals("EmailValidator", true, validator.validate(product));
    }

    @Test
    public void validaTest2() {
        product.setEmail("@mail@mail.ru");
        assertEquals("EmailValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setEmail("mail@mailru");
        assertEquals("EmailValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest4() {
        product.setEmail("mailmail.ru");
        assertEquals("EmailValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest5() {
        product.setEmail("mail@mail.ru.com");
        assertEquals("EmailValidator", true, validator.validate(product));

    }
}
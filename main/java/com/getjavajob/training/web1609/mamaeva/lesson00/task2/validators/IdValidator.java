package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

public class IdValidator implements Validator {
    public boolean validate(Product product) {
        String id = product.getId();
        return id != null && id.length() > 0 && id.codePoints().allMatch(Character::isUpperCase);
    }
}

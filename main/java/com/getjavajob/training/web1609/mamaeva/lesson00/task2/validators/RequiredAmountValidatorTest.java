package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RequiredAmountValidatorTest {
    private static Product product;
    private Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        product.setAvailableAmount(10);
        validator = new RequiredAmountValidator();
    }

    @Test
    public void validateTest() {
        product.setRequiredAmount(5);
        assertEquals("RequiredAmountValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setRequiredAmount(0);
        assertEquals("RequiredAmountValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setRequiredAmount(-123);
        assertEquals("RequiredAmountValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest4() {
        product.setRequiredAmount(11);
        assertEquals("RequiredAmountValidator", false, validator.validate(product));
    }
}
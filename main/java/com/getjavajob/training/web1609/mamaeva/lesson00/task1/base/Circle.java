package com.getjavajob.training.web1609.mamaeva.lesson00.task1.base;

public class Circle implements ClosedRegularFigure2D {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double baseArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getRadius() {
        return radius;
    }
}

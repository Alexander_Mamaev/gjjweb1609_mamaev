package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NameValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new NameValidator();
    }

    @Test
    public void validateTest() {
        product.setName("name");
        assertEquals("NameValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setName("");
        assertEquals("NameValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setName(null);
        assertEquals("NameValidator", false, validator.validate(product));
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

public class DiscountValidator implements Validator {
    private static final double DISCOUNT_LIMIT = 0.2;

    public  boolean validate(Product product) {
        return product.getDiscount() > 0 && product.getDiscount() < product.getRequiredAmount() * product.getPrice() * DISCOUNT_LIMIT;
    }
}

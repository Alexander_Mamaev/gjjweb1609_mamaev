package com.getjavajob.training.web1609.mamaeva.lesson00.task1;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure.*;

import static com.getjavajob.training.web1609.mamaeva.util.ConsoleReader.readDouble;
import static com.getjavajob.training.web1609.mamaeva.util.ConsoleReader.readInt;

public class FigureCalculator {
    public static void main(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("wrong input parameter.");
        }
        switch (args[0]) {
            case ("1"):
                calculateCone();
                break;
            case ("2"):
                calculatePyramid();
                break;
            case ("3"):
                calculateCylinder();
                break;
            case ("4"):
                calculatePrism();
                break;
            default:
                throw new IllegalArgumentException("wrong input parameter");
        }
    }

    private static void calculatePrism() {
        System.out.println("Please enter the number of angles of the prism base:");
        int angleNumber = readInt(3, 1000);
        System.out.println("Please enter the length of edge of the prism base:");
        double edgeLength = readDouble(1, 1000);
        System.out.println("Please enter the height of the prism:");
        double height = readDouble(1, 1000);
        System.out.println("Please enter the height of the prism cross section");
        double crossSectionHeight = readDouble(1, 1000);

        VolumeRegularFigure prism = new Prism(angleNumber, edgeLength, height);
        System.out.println(prism);
        System.out.printf("crossSectionArea at height of %s = %s", crossSectionHeight, prism.crossSectionArea(crossSectionHeight));
        System.out.println();
    }

    private static void calculateCylinder() {
        System.out.println("Please enter the radius of the cylinder base:");
        double radius = readDouble(1, 1000);
        System.out.println("Please enter the height of the cylinder:");
        double height = readDouble(1, 1000);
        System.out.println("Please enter the height of the cylinder cross section");
        double crossSectionHeight = readDouble(1, 1000);

        VolumeRegularFigure cylinder = new Cylinder(radius, height);
        System.out.println(cylinder);
        System.out.printf("crossSectionArea at height of %s = %s", crossSectionHeight, cylinder.crossSectionArea(crossSectionHeight));
        System.out.println();
    }

    private static void calculatePyramid() {
        System.out.println("Please enter the number of angles of the pyramid base:");
        int angleNumber = readInt(3, 1000);
        System.out.println("Please enter the length of edge of the pyramid base:");
        double edgeLength = readDouble(1, 1000);
        System.out.println("Please enter the height of the pyramid:");
        double height = readDouble(1, 1000);
        System.out.println("Please enter the height of the pyramid cross section");
        double crossSectionHeight = readDouble(1, 1000);

        VolumeRegularFigure pyramid = new Pyramid(angleNumber, edgeLength, height);
        System.out.println(pyramid);
        System.out.printf("crossSectionArea at height of %s = %s", crossSectionHeight, pyramid.crossSectionArea(crossSectionHeight));
        System.out.println();
    }

    public static void calculateCone() {
        System.out.println("Please enter the radius of the cone base:");
        double radius = readDouble(1, 1000);
        System.out.println("Please enter the height of the cone:");
        double height = readDouble(1, 1000);
        System.out.println("Please enter the height of the cone cross section");
        double crossSectionHeight = readDouble(1, 1000);

        VolumeRegularFigure cone = new Cone(radius, height);
        System.out.println(cone);
        System.out.printf("crossSectionArea at height of %s = %s", crossSectionHeight, cone.crossSectionArea(crossSectionHeight));
        System.out.println();
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.ClosedRegularFigure2D;

public abstract class RegularPrisms extends VolumeRegularFigure {
    public RegularPrisms(ClosedRegularFigure2D base, double height) {
        super(base, height);
    }

    @Override
    public double volume() {
        return baseArea() * getHeight();
    }

    @Override
    public double crossSectionArea(double crossSectionHeight) {
        return baseArea();
    }

    @Override
    public double surfaceArea() {
        return perimeter() * getHeight() + baseArea();
    }
}

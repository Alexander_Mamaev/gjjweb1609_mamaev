package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IdValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new IdValidator();
    }

    @Test
    public void validateTest() {
        product.setId("ИП");
        assertEquals("IdValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setId(null);
        assertEquals("IdValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setId("Ип");
        assertEquals("IdValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest4() {
        product.setId("");
        assertEquals("IdValidator", false, validator.validate(product));
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhoneNumberValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new PhoneNumberValidator();
    }

    @Test
    public void validateTest() {
        product.setPhoneNumber("+79123456789");
        assertEquals("PhoneNumberValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setPhoneNumber("079123456789");
        assertEquals("PhoneNumberValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setPhoneNumber("+89123456789");
        assertEquals("PhoneNumberValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest4() {
        product.setPhoneNumber("+7912345678");
        assertEquals("PhoneNumberValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest5() {
        product.setPhoneNumber("+7(912)34567");
        assertEquals("PhoneNumberValidator", false, validator.validate(product));
    }
}
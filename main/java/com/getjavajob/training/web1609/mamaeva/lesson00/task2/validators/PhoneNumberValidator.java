package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

import java.util.regex.Pattern;

public class PhoneNumberValidator implements Validator {
    public boolean validate(Product product) {
        return Pattern.compile("^\\+7{1}9{1}\\d{9}$").matcher(product.getPhoneNumber()).matches();
    }
}

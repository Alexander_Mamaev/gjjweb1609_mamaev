package com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.ClosedRegularFigure2D;

public abstract class VolumeRegularFigure implements RegularFigure3D {
    private ClosedRegularFigure2D base;
    private double height;

    public VolumeRegularFigure(ClosedRegularFigure2D base, double height) {
        this.height = height;
        this.base = base;
    }

    @Override
    public double baseArea() {
        return base.baseArea();
    }

    @Override
    public double perimeter() {
        return base.perimeter();
    }

    @Override
    public double getRadius() {
        return base.getRadius();
    }

    public ClosedRegularFigure2D getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "baseArea: " + baseArea() + System.lineSeparator() +
                "surfaceArea: " + surfaceArea() + System.lineSeparator() +
                "volume: " + volume();
    }
}

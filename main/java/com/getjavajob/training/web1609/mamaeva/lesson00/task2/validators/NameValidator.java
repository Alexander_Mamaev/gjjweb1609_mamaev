package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

public class NameValidator implements Validator {
    public boolean validate(Product product) {
        return product.getName() != null && product.getName().length() > 0;
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson00.task2;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators.*;

public class ProductValidator {
    private Validator[] validators;

    public ProductValidator(Validator[] validationType) {
        this.validators = validationType;
    }

    public Validator[] getValidators() {
        return validators;
    }

    public void setValidators(Validator[] validators) {
        this.validators = validators;
    }

    public boolean validate(Product product) {
        for(Validator v: validators) {
            if (!v.validate(product)) {
                return false;
            }
        }
        return true;
    }
}

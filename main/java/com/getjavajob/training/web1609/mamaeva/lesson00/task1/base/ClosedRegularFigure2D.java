package com.getjavajob.training.web1609.mamaeva.lesson00.task1.base;

public interface ClosedRegularFigure2D {
    /**
     * @return the area of figure
     */
    double baseArea();

    /**
     * @return the perimeter of figure
     */
    double perimeter();

    /**
     * @return the radius of inscribed circle
     */
    double getRadius();
}

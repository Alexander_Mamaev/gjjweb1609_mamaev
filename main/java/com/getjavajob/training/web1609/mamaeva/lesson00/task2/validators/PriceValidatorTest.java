package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PriceValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new PriceValidator();
    }

    @Test
    public void validateTest() {
        product.setPrice(12);
        assertEquals("PriceValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setPrice(0);
        assertEquals("PriceValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setPrice(-56);
        assertEquals("PriceValidator", false, validator.validate(product));
    }
}
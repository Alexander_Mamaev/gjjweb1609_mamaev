package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiscountValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        product.setRequiredAmount(5);
        product.setPrice(35);
        validator = new DiscountValidator();
    }

    @Test
    public void validateTest() {
        product.setDiscount(5);
        assertEquals("DiscountValidator", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        product.setDiscount(0);
        assertEquals("DiscountValidator", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        product.setDiscount(35);
        assertEquals("DiscountValidator", false, validator.validate(product));
    }
}
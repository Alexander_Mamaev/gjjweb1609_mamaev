package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class DeliveryDateValidatorTest {
    private static SimpleDateFormat df;
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        df = new SimpleDateFormat("dd.MM.yyyy");
        product = new Product();
        validator = new DeliveryDateValidator();
    }

    @Test
    public void validateTest1() {
        Calendar acceptedDate = new GregorianCalendar();
        acceptedDate.add(Calendar.DAY_OF_MONTH, 1);
        product.setDeliveryDate(df.format(acceptedDate.getTime()));
        assertEquals("acceptedDate", true, validator.validate(product));
    }

    @Test
    public void validateTest2() {
        Calendar currentDate = new GregorianCalendar();
        product.setDeliveryDate(df.format(currentDate.getTime()));
        assertEquals("currentDate", false, validator.validate(product));
    }

    @Test
    public void validateTest3() {
        Calendar rejectedDate = new GregorianCalendar();
        rejectedDate.add(Calendar.DAY_OF_MONTH, -5);
        product.setDeliveryDate(df.format(rejectedDate.getTime()));
        assertEquals("rejectedDate", false, validator.validate(product));
    }
}
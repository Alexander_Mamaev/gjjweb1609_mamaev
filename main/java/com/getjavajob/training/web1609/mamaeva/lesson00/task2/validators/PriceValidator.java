package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

public class PriceValidator implements Validator {
    public boolean validate(Product product) {
        return product.getPrice() > 0;
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.RegularPolyhedron;

public class Pyramid extends RegularPyramids {
    public Pyramid(int angleNumber, double edgeLength, double height) {
        super(new RegularPolyhedron(angleNumber, edgeLength), height);
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DeliveryDateValidator implements Validator {
    public boolean validate(Product product) {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Calendar date = new GregorianCalendar();
        try {
            date.setTime(df.parse(product.getDeliveryDate()));
        } catch (ParseException e) {
            System.out.println("Wrong date format!");
        }

        Calendar tomorrow = new GregorianCalendar();
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);
        tomorrow.set(Calendar.SECOND, 0);
        tomorrow.set(Calendar.MILLISECOND, 0);
        return date.compareTo(tomorrow) >= 0;
    }
}

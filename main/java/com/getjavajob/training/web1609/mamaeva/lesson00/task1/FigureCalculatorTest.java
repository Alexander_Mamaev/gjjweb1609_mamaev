package com.getjavajob.training.web1609.mamaeva.lesson00.task1;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.Circle;
import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.RegularPolyhedron;
import com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

public class FigureCalculatorTest {
    @Test
    public void mainTest() throws Exception {
        String input = "5\r\n10\r\n5\r\n" +
                "6\r\n3\r\n10\r\n5\r\n" +
                "5\r\n10\r\n5\r\n" +
                "6\r\n3\r\n10\r\n5";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        FigureCalculator.main(new String[]{"1"});
        System.out.println();
        FigureCalculator.main(new String[]{"2"});
        System.out.println();
        FigureCalculator.main(new String[]{"3"});
        System.out.println();
        FigureCalculator.main(new String[]{"4"});

        System.setIn(System.in);
    }

    @Test
    public void coneTest() {
        VolumeRegularFigure cone = new Cone(5, 10);
        Assert.assertEquals("cone base area", 78.5, cone.baseArea(), 0.05);
        Assert.assertEquals("cone getBase()", Circle.class, cone.getBase().getClass());
        Assert.assertEquals("cone cross section area", 19.6, cone.crossSectionArea(5), 0.05);
        Assert.assertEquals("cone surface area", 254.2, cone.surfaceArea(), 0.05);
        Assert.assertEquals("cone volume", 261.8, cone.volume(), 0.05);
        cone.setHeight(11);
        Assert.assertEquals("cone set/getHeight()", 11, cone.getHeight(), 0.0001);
        Assert.assertEquals("cone volume", 288, cone.volume(), 0.05);
        Assert.assertEquals("cone getRadius()", 5, cone.getRadius(), 0.0001);
    }

    @Test
    public void pyramidTest() {
        VolumeRegularFigure pyramid = new Pyramid(6, 3, 10);
        Assert.assertEquals("pyramid base area", 23.4, pyramid.baseArea(), 0.05);
        Assert.assertEquals("pyramid getBase()", RegularPolyhedron.class, pyramid.getBase().getClass());
        Assert.assertEquals("pyramid cross section area", 5.8, pyramid.crossSectionArea(5), 0.05);
        Assert.assertEquals("pyramid surface area", 116.4, pyramid.surfaceArea(), 0.05);
        Assert.assertEquals("pyramid volume", 77.9, pyramid.volume(), 0.05);
        pyramid.setHeight(11);
        Assert.assertEquals("pyramid set/getHeight()", 11, pyramid.getHeight(), 0.0001);
        Assert.assertEquals("pyramid volume", 85.7, pyramid.volume(), 0.05);
        Assert.assertEquals("pyramid getRadius()", 2.6, pyramid.getRadius(), 0.05);
    }

    @Test
    public void cylinderTest() {
        VolumeRegularFigure cylinder = new Cylinder(5, 10);
        Assert.assertEquals("cylinder base area", 78.5, cylinder.baseArea(), 0.05);
        Assert.assertEquals("cylinder getBase()", Circle.class, cylinder.getBase().getClass());
        Assert.assertEquals("cylinder cross section area", 78.5, cylinder.crossSectionArea(5), 0.05);
        Assert.assertEquals("cylinder surface area", 392.7, cylinder.surfaceArea(), 0.05);
        Assert.assertEquals("cylinder volume", 785.4, cylinder.volume(), 0.05);
        cylinder.setHeight(11);
        Assert.assertEquals("cylinder set/getHeight()", 11, cylinder.getHeight(), 0.0001);
        Assert.assertEquals("cylinder volume", 863.9, cylinder.volume(), 0.05);
        Assert.assertEquals("cylinder getRadius()", 5, cylinder.getRadius(), 0.05);
    }

    @Test
    public void prismTest() {
        VolumeRegularFigure prism = new Prism(6, 3, 10);
        Assert.assertEquals("prism base area", 23.4, prism.baseArea(), 0.05);
        Assert.assertEquals("prism getBase()", RegularPolyhedron.class, prism.getBase().getClass());
        Assert.assertEquals("prism cross section area", 23.4, prism.crossSectionArea(5), 0.05);
        Assert.assertEquals("prism surface area", 203.4, prism.surfaceArea(), 0.05);
        Assert.assertEquals("prism volume", 233.8, prism.volume(), 0.05);
        prism.setHeight(11);
        Assert.assertEquals("prism set/getHeight()", 11, prism.getHeight(), 0.0001);
        Assert.assertEquals("prism volume", 257.2, prism.volume(), 0.05);
        Assert.assertEquals("prism getRadius()", 2.6, prism.getRadius(), 0.05);
    }
}
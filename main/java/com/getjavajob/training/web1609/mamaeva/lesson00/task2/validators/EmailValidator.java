package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

import java.util.regex.Pattern;

public class EmailValidator implements Validator {
    public boolean validate(Product product) {
        return Pattern.compile("^[^@]+@[^@.]+\\.[^@]+$").matcher(product.getEmail()).matches();
    }
}

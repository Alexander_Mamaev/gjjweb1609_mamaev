package com.getjavajob.training.web1609.mamaeva.lesson00.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson00.task1.base.Circle;

public class Cone extends RegularPyramids {
    public Cone(double radius, double height) {
        super(new Circle(radius), height);
    }
}

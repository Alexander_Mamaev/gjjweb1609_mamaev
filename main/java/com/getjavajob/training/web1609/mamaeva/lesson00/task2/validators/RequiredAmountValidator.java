package com.getjavajob.training.web1609.mamaeva.lesson00.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson00.task2.Product;

public class RequiredAmountValidator implements Validator {
    public boolean validate(Product product) {
        return product.getRequiredAmount() > 0 && product.getRequiredAmount() < product.getAvailableAmount();
    }
}

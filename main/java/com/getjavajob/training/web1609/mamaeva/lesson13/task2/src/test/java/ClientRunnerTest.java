import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This DB used for testing:
 * DB name 'lesson13'
 * table name 'employee'
 * +----+-------+-----------+--------------+
 * | id | name  | surname   | phone        |
 * +----+-------+-----------+--------------+
 * |  1 | Ivan  | Ivanov    | +71111111111 |
 * |  2 | Petr  | Petrov    | +72222222222 |
 * |  3 | Vasya | Vasechkin | +73333333333 |
 * +----+-------+-----------+--------------+
 * <p>
 * DB creation script:
 * create database lesson13;
 * <p>
 * create table employee (
 * id int primary key auto_increment,
 * name varchar(20) not null,
 * surname varchar(50) not null,
 * phone varchar(20) not null);
 * <p>
 * insert into employee(name, surname, phone) values
 * ('Ivan', 'Ivanov', '+71111111111'),
 * ('Petr', 'Petrov', '+72222222222'),
 * ('Vasya', 'Vasechkin', '+73333333333');
 */
public class ClientRunnerTest {
    private ClientRunner clientRunner = new ClientRunner();
    private ClientDAO dao = new ClientDAO();

    @Before
    public void setUp() throws Exception {
        clientRunner.request(dao, "use lesson13;");
    }

    @Test
    public void showDB() {
        String request = "show databases;";
        String actual = clientRunner.request(dao, request);
        assertEquals("showDB: ", true, actual.contains("lesson13"));
    }

    @Test
    public void selectInitData() {
        String expected = "id  |  name  |  surname  |  phone  |  \r\n" +
                "---------------------------\r\n" +
                "1  |  Ivan  |  Ivanov  |  +71111111111  |  \r\n" +
                "2  |  Petr  |  Petrov  |  +72222222222  |  \r\n" +
                "3  |  Vasya  |  Vasechkin  |  +73333333333  |  \r\n";
        String request = "select id, name, surname, phone from employee where id IN (1, 2, 3);";
        assertEquals(request, expected, clientRunner.request(dao, request));
    }

    @Test
    public void insertAndDelete() {
        String request0 = "delete from employee where id = 10;";
        String expected = "ok" + System.lineSeparator();
        assertEquals("insertAndDelete - primary: ", expected, clientRunner.request(dao, request0));

        String request = "insert into employee (id, name, surname, phone) values (10, 'name10', 'surname10', 'phone10')";
        assertEquals("insertAndDelete - insert: ", expected, clientRunner.request(dao, request));

        String request2 = "select id, name, surname, phone from employee where id = 10;";
        String expected2 = "id  |  name  |  surname  |  phone  |  \r\n" +
                "---------------------------\r\n" +
                "10  |  name10  |  surname10  |  phone10  |  \r\n";
        assertEquals("insertAndDelete - select: ", expected2, clientRunner.request(dao, request2));

        assertEquals("insertAndDelete - secondary: ", expected, clientRunner.request(dao, request0));

        String expected3 = "id  |  name  |  surname  |  phone  |  \r\n" +
                "---------------------------\r\n";
        assertEquals("insertAndDelete - select after deletion: ", expected3, clientRunner.request(dao, request2));
    }


    @Test
    public void createAndDropTable() {
        String request0 = "drop table tmp;";
        String expected = "transaction failed: Unknown table 'lesson13.tmp'";
        assertEquals("createAndDropTable - drop primary: ", expected, clientRunner.request(dao, request0));


        String request1 = "create table tmp (id int primary key auto_increment,\r\n" +
                "  name varchar(20) not null,\r\n" +
                "  surname varchar(50) not null,\r\n" +
                "  phone varchar(20) not null);";
        String expected1 = "ok" + System.lineSeparator();
        assertEquals("createAndDropTable - create: ", expected1, clientRunner.request(dao, request1));

        assertEquals("createAndDropTable - drop secondary: ", expected1, clientRunner.request(dao, request0));
    }
}
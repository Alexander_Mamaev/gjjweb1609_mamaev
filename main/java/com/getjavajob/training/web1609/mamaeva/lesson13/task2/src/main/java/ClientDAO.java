import java.io.IOException;
import java.sql.*;
import java.util.Arrays;
import java.util.Properties;

public class ClientDAO implements AutoCloseable {
    private Connection connection;
    private static final String[] DQLCommands = {"SELECT", "SHOW"};

    public ClientDAO() {
        try {
            Properties props = new Properties();
            props.load(this.getClass().getClassLoader().getResourceAsStream("lesson13.properties"));
             String driver = props.getProperty("database.driver");
            String url = props.getProperty("database.url");
            String user = props.getProperty("database.user");
            String password = props.getProperty("database.password");
             Class.forName(driver);
            this.connection = DriverManager.getConnection(url, user, password);
            this.connection.setAutoCommit(false);
        } catch (IOException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getTransaction(String request) {
        try (Statement st = this.connection.createStatement()) {
            if (Arrays.toString(DQLCommands).contains(request.toUpperCase().trim().split(" ")[0])) {
                ResultSet rs = st.executeQuery(request);
                return printResult(rs);
            } else {
                st.executeUpdate(request);
                this.connection.commit();
                return "ok" + System.lineSeparator();
            }
        } catch (SQLException e) {
            return "transaction failed: " + e.getMessage();
        } finally {
            try {
                this.connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String printResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        StringBuilder sb = new StringBuilder();

        int countCol = rsmd.getColumnCount();
        for (int i = 1; i <= countCol; i++) {
            sb.append(rsmd.getColumnName(i)).append("  |  ");
        }
        sb.append(System.lineSeparator()).append("---------------------------").append(System.lineSeparator());

        while (rs.next()) {
            for (int i = 1; i <= countCol; i++) {
                sb.append(rs.getString(i)).append("  |  ");
            }
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    @Override
    public void close() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

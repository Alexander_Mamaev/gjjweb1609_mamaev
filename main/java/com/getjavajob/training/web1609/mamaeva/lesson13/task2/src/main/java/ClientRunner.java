import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientRunner {
    private StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {
        ClientRunner clientRunner = new ClientRunner();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
             ClientDAO dao = new ClientDAO()) {
            while (true) {
                System.out.println("Ready for request (type 'exit' to finish).");
                String result = clientRunner.request(dao, clientRunner.getRequest(br));
                if ("exit".equals(result)) {
                    return;
                } else {
                    System.out.println(result);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String request(ClientDAO client, String request) {
        if (request.contains("exit")) {
            return "exit";
        }
        return client.getTransaction(request);
    }

    private String getRequest(BufferedReader br) throws IOException {
        sb.setLength(0);
        for (boolean flag = true; flag; ) {
            String s = br.readLine();
            if (s.contains(";") || s.contains("exit")) {
                flag = false;
            }
            sb.append(s).append(" ");
        }
        return sb.toString();
    }
}

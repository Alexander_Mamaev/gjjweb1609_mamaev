package com.getjavajob.training.web1609.mamaeva.lesson15.task3;

public interface MaxFinder {
    public abstract int findMax(int[] array);
}

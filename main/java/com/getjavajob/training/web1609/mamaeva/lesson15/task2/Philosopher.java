package com.getjavajob.training.web1609.mamaeva.lesson15.task2;

public class Philosopher extends Thread {
    private Steward steward;
    private Fork leftFork;
    private Fork rightFork;
    private int eatTime;
    private int thinkTime;
    private int eatCounter;
    private int thinkCounter;
    private String philName;

    public Philosopher(Fork leftFork, Fork rightFork, Steward steward, int eatTime, int thinkTime) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.steward = steward;
        this.eatTime = eatTime;
        this.thinkTime = thinkTime;
    }

    @Override
    public void run() {
        philName = Thread.currentThread().getName();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                steward.acquire();
                take(leftFork);
                take(rightFork);
                eat();
                put(leftFork);
                put(rightFork);
                steward.release();
                think();
            } catch (InterruptedException e) {
//                do nothing
            } finally {
                steward.release();
            }
        }
    }

    private void take(Fork fork) {
        fork.take();
    }

    private void put(Fork fork) {
        fork.put();
    }

    private void eat() {
//        System.err.println(Thread.currentThread().getName() + " ем");
        try {
            Thread.sleep(eatTime);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
//        System.err.println(Thread.currentThread().getName() + " закончил есть");
        eatCounter += eatTime;
    }

    private void think() {
//        System.err.println(Thread.currentThread().getName() + " думаю");
        try {
            Thread.sleep(thinkTime);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
//        System.err.println(Thread.currentThread().getName() + " закончил думать");
        thinkCounter += thinkTime;
    }

    @Override
    public String toString() {
        return "Philosopher " + philName +" {" +
                "eatCounter=" + eatCounter +
                ", thinkCounter=" + thinkCounter +
                '}';
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson15.task3;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class FindMaxClassTest {
    private static int[] expectedMinimum;
    private MaxFinder smf;
    private MaxFinder pmf;
    private final int largeArraySize = 50_000_000;

    @Before
    public void before() {
        smf = new SingleMaxFinder();
        pmf = new ForkJoinMaxFinder();
        expectedMinimum = new int[]{2, 4, 6, 8, 0, 9, 7, 5, 3, 1, 0, 2, 4, 6, 8, 0, 9, 7, 5, 3, 1};
    }

    @Test
    public void findMaxSimpleTest() {
        int r = pmf.findMax(expectedMinimum);
        assertEquals(9, r);
    }

    @Test
    public void findMaxSingleTest() {
        int r = smf.findMax(expectedMinimum);
        assertEquals(9, r);
    }

    @Test
    public void findMaxTest() {
        int[] array = fillArray(largeArraySize);
        System.out.println("----------Start---------");
        long time = System.nanoTime();
        int singleResult = smf.findMax(array);
        System.out.println("SingleMaxFinder works : " + (System.nanoTime() - time) / 1000);
        System.out.println("Result is " + singleResult);

        time = System.nanoTime();
        int forkJoinResult = pmf.findMax(array);
        System.out.println("ForkJoinMaxFinder works: " + (System.nanoTime() - time) / 1000);
        System.out.println("Result is " + forkJoinResult);

        assertEquals(singleResult, forkJoinResult);
    }

    private int[] fillArray(int numberElements) {
        int[] array = new int[numberElements];
        Arrays.parallelSetAll(array, i -> new Random().nextInt());
        return array;
    }

}
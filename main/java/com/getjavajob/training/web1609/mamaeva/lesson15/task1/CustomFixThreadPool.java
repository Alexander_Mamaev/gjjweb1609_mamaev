package com.getjavajob.training.web1609.mamaeva.lesson15.task1;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class CustomFixThreadPool extends Thread {
    private BlockingQueue<Runnable> workQueue;
    private Thread[] workers;


    public CustomFixThreadPool(int capacity) {
        workers = new WorkerThread[capacity];
        workQueue = new LinkedBlockingDeque<>();

        for (int i = 0; i < capacity; i++) {
            workers[i] = new WorkerThread();
            workers[i].start();
        }
    }

    public void execute(Runnable task) {
        try {
            workQueue.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class WorkerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("worker :" + Thread.currentThread().getName() + "start ");
                    workQueue.take().run();
                    System.out.println("worker :" + Thread.currentThread().getName() + "finished ");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

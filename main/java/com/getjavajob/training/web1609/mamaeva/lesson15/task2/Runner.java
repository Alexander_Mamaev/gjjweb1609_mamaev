package com.getjavajob.training.web1609.mamaeva.lesson15.task2;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    private static final int numberOfPhilosophers = 5;
    private static final int eatTime = 100;
    private static final int thinkTime = 50;
    private static final int operationTime = 5000;

    public static void main(String[] args) {
//Назначаем стюарда
        Steward steward = new Steward(numberOfPhilosophers / 2);

//Раздаем вилки
        List<Fork> forks = new ArrayList<>();
        for (int i = 0; i < numberOfPhilosophers; i++) {
            forks.add(new Fork());
        }

//Садим философов
        List<Philosopher> philosophers = new ArrayList<>();
        for (int i = 0; i < numberOfPhilosophers; i++) {
            Fork left = forks.get(i);
            Fork right = forks.get((i + 1) % numberOfPhilosophers);
            philosophers.add(new Philosopher(left, right, steward, eatTime, thinkTime));
        }

//Разрешаем поесть
        for (int i = 0; i < numberOfPhilosophers; i++) {
            philosophers.get(i).start();
        }

//Определяем время жизни философов
        try {
            Thread.sleep(operationTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        philosophers.forEach(Thread::interrupt);

//Ждем когда все "закончат"
        for (Philosopher p : philosophers) {
            try {
                p.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//Выводим результат деятельности философов
        for (Philosopher p : philosophers) {
            System.out.println(p.toString());
        }
    }
}

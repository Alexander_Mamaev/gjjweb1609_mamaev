package com.getjavajob.training.web1609.mamaeva.lesson15.task2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Fork {
    private Lock lock = new ReentrantLock();

    public void take() {
        lock.lock();
    }

    public void put() {
        lock.unlock();
    }
}

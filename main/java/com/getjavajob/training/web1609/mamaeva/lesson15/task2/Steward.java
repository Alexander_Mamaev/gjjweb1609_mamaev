package com.getjavajob.training.web1609.mamaeva.lesson15.task2;

import java.util.concurrent.Semaphore;

public class Steward extends Semaphore{
    public Steward(int permits) {
        super(permits);
    }

    public Steward(int permits, boolean fair) {
        super(permits, fair);
    }
}

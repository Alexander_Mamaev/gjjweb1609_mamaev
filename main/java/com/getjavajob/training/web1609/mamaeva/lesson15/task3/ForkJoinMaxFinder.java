package com.getjavajob.training.web1609.mamaeva.lesson15.task3;

import java.util.concurrent.RecursiveTask;

public class ForkJoinMaxFinder implements MaxFinder {
    private final int THRESHOLD;

    public ForkJoinMaxFinder() {
        THRESHOLD = 1_000_000;
    }

    public ForkJoinMaxFinder(int THRESHOLD) {
        this.THRESHOLD = THRESHOLD;
    }

    @Override
    public int findMax(int[] array) {
        SearchMaxTask smt = new SearchMaxTask(array, 0, array.length);
        smt.fork();
        return smt.join();
    }

    private class SearchMaxTask extends RecursiveTask<Integer> {
        private int[] array;
        private int start;
        private int end;

        public SearchMaxTask(int[] array, int start, int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }

        @Override
        protected Integer compute() {
            int middle = (end - start) / 2;
            if (middle < THRESHOLD) {
                return new SingleMaxFinder().findMax(array, start, end);
            }
            SearchMaxTask t1 = new SearchMaxTask(array, start, start + middle);
            SearchMaxTask t2 = new SearchMaxTask(array, start + middle, end);
            t1.fork();
            t2.fork();
            return Math.max(t1.join(), t2.join());
        }
    }
}

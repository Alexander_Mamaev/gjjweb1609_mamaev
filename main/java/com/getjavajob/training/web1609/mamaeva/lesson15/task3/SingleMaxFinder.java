package com.getjavajob.training.web1609.mamaeva.lesson15.task3;

public class SingleMaxFinder implements MaxFinder {

    @Override
    public int findMax(int[] array) {
        return findMax(array, 0, array.length);
    }

    protected int findMax(int[] array, int start, int end) {
        int r = array[start];
        for (int i = start + 1; i < end; i++) {
            r = Math.max(r, array[i]);
        }
        return r;
    }
}

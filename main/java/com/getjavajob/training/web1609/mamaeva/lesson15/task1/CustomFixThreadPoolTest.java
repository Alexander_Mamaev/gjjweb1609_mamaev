package com.getjavajob.training.web1609.mamaeva.lesson15.task1;

import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;


public class CustomFixThreadPoolTest {
    private final AtomicInteger counter = new AtomicInteger();
    private Set<Integer> set;
    private CustomFixThreadPool pool;

    @Test
    public void test() {
        set = Collections.synchronizedSet(new HashSet<>());
        pool = new CustomFixThreadPool(4);

        processing(pool);
        System.out.println("----- some sleeping ---------");
        processing(pool);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("The END.");

        Set<Integer> expected = new HashSet<>();
        for (int i = 1; i <= 20; i++) {
            expected.add(i);
        }

        assertEquals(expected, set);
    }

    private void processing(CustomFixThreadPool pool) {
        for (int i = 0; i < 10; i++) {
            pool.execute(new Task());
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Task implements Runnable {
        @Override
        public void run() {
            int count = counter.incrementAndGet();
            System.out.println(count + " : " + Thread.currentThread().getName() + " : working");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            set.add(count);
            System.out.println(count + " : " + Thread.currentThread().getName() + " : finished");
        }
    }
}


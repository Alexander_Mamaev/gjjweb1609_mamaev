package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getBiggestOrgInfo;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getRichestShareholderInfo;
import static org.junit.Assert.assertEquals;

public class SAXParserTest {
    private OrganizationsList orgs;

    @Before
    public void setUp() {

        try (InputStream is = SAXParserTest.class.getResourceAsStream("Organizations.xml")) {
            orgs = new SAXParser().parseOrganizations(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parseOrganizations() throws Exception {
        assertEquals("Size of orgs", 4, orgs.asMap().size());
        assertEquals("getBiggestOrgInfo", "Org1", getBiggestOrgInfo(orgs));
        assertEquals("getRichestShareholderInfo", "ShName3", getRichestShareholderInfo(orgs));
    }

}
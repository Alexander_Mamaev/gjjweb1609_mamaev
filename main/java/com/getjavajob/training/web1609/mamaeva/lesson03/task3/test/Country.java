package com.getjavajob.training.web1609.mamaeva.lesson03.task3.test;

import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAlias;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAttribute;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLTransient;

@XMLAlias(str = "country_land")
public class Country {
    @XMLAttribute()
    private String countryName;
    @XMLAlias(str = "Part_of_the_world")
    private WorldParts worldParts;
    private String extraField1;
    @XMLTransient
    private short extraField2;

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setWorldParts(WorldParts worldParts) {
        this.worldParts = worldParts;
    }

    public void setExtraField1(String extraField1) {
        this.extraField1 = extraField1;
    }

    public void setExtraField2(short extraField2) {
        this.extraField2 = extraField2;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", worldParts=" + worldParts +
                ", extraField1='" + extraField1 + '\'' +
                ", extraField2=" + extraField2 +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        if (extraField2 != country.extraField2) return false;
        if (countryName != null ? !countryName.equals(country.countryName) : country.countryName != null) return false;
        if (worldParts != null ? !worldParts.equals(country.worldParts) : country.worldParts != null) return false;
        return extraField1 != null ? extraField1.equals(country.extraField1) : country.extraField1 == null;

    }

    @Override
    public int hashCode() {
        int result = countryName != null ? countryName.hashCode() : 0;
        result = 31 * result + (worldParts != null ? worldParts.hashCode() : 0);
        result = 31 * result + (extraField1 != null ? extraField1.hashCode() : 0);
        result = 31 * result + (int) extraField2;
        return result;
    }
}

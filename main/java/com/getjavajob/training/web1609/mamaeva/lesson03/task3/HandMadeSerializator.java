package com.getjavajob.training.web1609.mamaeva.lesson03.task3;


import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAlias;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAttribute;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLTransient;

import java.io.*;
import java.lang.reflect.Field;

public class HandMadeSerializator {
    private static int tabCounter;

    //    ------------------------------------- serialization -----------------------------------------
    public static void toXML(Object o, OutputStream os) throws IllegalAccessException, NoSuchFieldException, IOException {
        StringBuilder sb = new StringBuilder();
        readClass(o, sb);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        bw.write(sb.toString());
        bw.flush();
//        System.out.println(sb.toString());
    }

    private static void readClass(Object o, StringBuilder sb) throws IllegalAccessException, NoSuchFieldException {
        String classTag = getClassTagName(o);
        sb.append("<").append(classTag).append(">");
        Field[] fields = o.getClass().getDeclaredFields();
        tabCounter++;
        for (Field f : fields) {
            addFieldTag(o, f, sb);
        }
        tabCounter--;
        addNextLine(sb);
        sb.append("</").append(classTag.split(" ")[0]).append(">");
    }

    private static String getClassTagName(Object o) throws IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        if (o.getClass().isAnnotationPresent(XMLAlias.class)) {
            sb.append(o.getClass().getAnnotation(XMLAlias.class).str());
        } else {
            sb.append(o.getClass().getName());
        }

        Field[] fields = o.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(XMLAttribute.class)) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(XMLAlias.class)) {
                    sb.append(" ").append(f.getAnnotation(XMLAlias.class).str()).append("=").append("\"").append(f.get(o)).append("\"");
                } else {
                    sb.append(" ").append(f.getName()).append("=").append("\"").append(f.get(o)).append("\"");
                }
            }
        }
        return sb.toString();
    }

    private static void addFieldTag(Object o, Field f, StringBuilder sb) throws IllegalAccessException, NoSuchFieldException {
        f.setAccessible(true);
        String fieldName = getFieldTagName(f);
        if (fieldName == null) {
            return;
        }
        if (f.getType().isPrimitive() || f.getType().equals(String.class)) {
            addNextLine(sb);
            sb.append("<").append(fieldName).append(">").append(f.get(o)).append("</").append(fieldName).append(">");
        } else {
            addNextLine(sb);
            tabCounter++;
            sb.append("<").append(fieldName).append(">");
            addNextLine(sb);
            Object ob = f.get(o);
            readClass(ob, sb);
            tabCounter--;
            addNextLine(sb);
            sb.append("</").append(fieldName).append(">");
        }
    }

    private static String getFieldTagName(Field f) {
        if (f.isAnnotationPresent(XMLTransient.class) || f.isAnnotationPresent(XMLAttribute.class)) {
            return null;
        } else if (f.isAnnotationPresent(XMLAlias.class)) {
            return f.getAnnotation(XMLAlias.class).str();
        } else {
            return f.getName();
        }
    }

    //    ------------------------------------- deserialization -----------------------------------------
    public static Object fromXML(InputStream is, Class c) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, NoSuchFieldException {
        Object o = c.newInstance();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while (br.ready()) {
            String attr = br.readLine().trim();
            o = readObjectFromXML(br, o);
            setAttributes(attr, o);
        }
        return o;
    }

    private static Object readObjectFromXML(BufferedReader br, Object o) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        while (br.ready()) {
            String l = br.readLine().trim();
            String[] str = l.split("<|>");

            if (!l.startsWith("</") && l.contains("</")) {
                Field f = getFieldFromTagName(str[1], o);
                if (f != null) {
                    f.setAccessible(true);
                    if (f.getGenericType().equals(String.class)) {
                        setFieldValue(o, str[2], f);
                    } else if (f.getGenericType().equals(int.class)) {
                        f.set(o, Integer.parseInt(str[2]));
                    } else if (f.getGenericType().equals(double.class)) {
                        f.set(o, Double.parseDouble(str[2]));
                    } else if (f.getGenericType().equals(float.class)) {
                        f.set(o, Float.parseFloat(str[2]));
                    } else if (f.getGenericType().equals(long.class)) {
                        f.set(o, Long.parseLong(str[2]));
                    } else if (f.getGenericType().equals(byte.class)) {
                        f.set(o, Byte.parseByte(str[2]));
                    } else if (f.getGenericType().equals(short.class)) {
                        f.set(o, Short.parseShort(str[2]));
                    } else if (f.getGenericType().equals(boolean.class)) {
                        f.set(o, Boolean.parseBoolean(str[2]));
                    }
                }
            } else if (l.startsWith("<") && !l.contains("</")) {
                Field f = getFieldFromTagName(str[1], o);
                if (f != null) {
                    f.setAccessible(true);
                    String attr = br.readLine().trim();
                    Class c = Class.forName(f.getGenericType().getTypeName());
                    Object ob = readObjectFromXML(br, c.newInstance());
                    f.set(o, ob);
                    setAttributes(attr, ob);
                }
            } else if (l.startsWith("</")) {
                return o;
            }
        }
        return o;
    }

    private static void setFieldValue(Object o, String value, Field f) throws IllegalAccessException {
        if ("null".equals(value)) {
            f.set(o, null);
        } else {
            f.set(o, value);
        }
    }

    private static void setAttributes(String attr, Object o) throws IllegalAccessException {
        if (!attr.contains("</")) {
            String[] a = attr.replace(">", "").replace("\"", "").split(" ");
            for (int i = 1; i < a.length; i++) {
                String[] field = a[i].split("=");
                Field f = getFieldFromTagName(field[0], o);
                f.setAccessible(true);
                setFieldValue(o, field[1], f);
            }
        }
    }

    private static Field getFieldFromTagName(String tagName, Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(XMLTransient.class) &
                    (f.isAnnotationPresent(XMLAlias.class) && f.getAnnotation(XMLAlias.class).str().equals(tagName) ||
                            f.getName().equals(tagName))) {
                return null;
            } else if (f.isAnnotationPresent(XMLAlias.class) &&
                    f.getAnnotation(XMLAlias.class).str().equals(tagName)) {
                return f;
            } else if (f.getName().equals(tagName)) {
                return f;
            }
        }
        return null;
    }

    private static void addNextLine(StringBuilder sb) {
        sb.append(System.lineSeparator());
        for (int i = 0; i < tabCounter; i++) {
//            sb.append("\u0009");
            sb.append("    ");
        }
    }
}
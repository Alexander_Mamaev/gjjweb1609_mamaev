package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Organization;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Person;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Shareholder;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

public class StAXParser {
    private OrganizationsList orgs;
    private Organization org;
    private Person person;
    private Shareholder shareholder;

    public OrganizationsList parseOrganizations(InputStream is) throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(is);

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "organizations":
                            orgs = new OrganizationsList();
                            break;
                        case "organization":
                            org = new Organization();
                            org.setName(reader.getAttributeValue(0));
                            orgs.add(org);
                            break;
                        case "legal_entity":
                            org.setLegalEntity(reader.getElementText().trim());
                            break;
                        case "ceo":
                            person = new Person();
                            org.setCeo(person);
                            break;
                        case "first_name":
                            person.setFirstName(reader.getElementText().trim());
                            break;
                        case "last_name":
                            person.setLastName(reader.getElementText().trim());
                            break;
                        case "patronymic":
                            person.setPatronymic(reader.getElementText().trim());
                            break;
                        case "authorized_capital":
                            org.setAuthorizedCapital(reader.getElementText().trim());
                            break;
                        case "org_parent":
                            org.setParent(orgs.getOrganization(reader.getElementText().trim()));
                            break;
                        case "shareholder":
                            shareholder = new Shareholder();
                            org.addShareholder(shareholder);
                            break;
                        case "sh_last_name":
                            shareholder.setLastName(reader.getElementText().trim());
                            break;
                        case "sh_percentage":
                            shareholder.setSharesPercentage(reader.getElementText().trim());
                            break;
                        default:
                            throw new IllegalArgumentException("default in switch case");
                    }
            }
        }
        return orgs;
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import org.junit.Before;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;

import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getBiggestOrgInfo;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getRichestShareholderInfo;
import static org.junit.Assert.assertEquals;

public class StAXParserTest {
    private OrganizationsList orgs;

    @Before
    public void setUp() {
        try (InputStream is = getClass().getResourceAsStream("Organizations.xml")) {
            orgs = new StAXParser().parseOrganizations(is);
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parseOrganizations() throws Exception {
        assertEquals("Size of orgs", 4, orgs.asMap().size());
        assertEquals("getBiggestOrgInfo", "Org1", getBiggestOrgInfo(orgs));
        assertEquals("getRichestShareholderInfo", "ShName3", getRichestShareholderInfo(orgs));
    }

}
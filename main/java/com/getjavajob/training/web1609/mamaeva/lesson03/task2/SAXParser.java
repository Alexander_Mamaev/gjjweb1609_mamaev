package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Organization;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Person;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Shareholder;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

public class SAXParser {

    public OrganizationsList parseOrganizations(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser = parserFactory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(is, handler);

        return handler.getOrgs();
    }
}

/**
 * The Handler for SAX Events.
 */
class SAXHandler extends DefaultHandler {

    private OrganizationsList orgs;
    private Organization org;
    private Shareholder shareholder;
    private Person ceo;
    private String content;

    public OrganizationsList getOrgs() {
        return orgs;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "organizations":
                orgs = new OrganizationsList();
                break;
            case "organization":
                org = new Organization();
                org.setName(attributes.getValue("name"));
                orgs.add(org);
                break;
            case "shareholder":
                shareholder = new Shareholder();
                org.addShareholder(shareholder);
                break;
            case "ceo":
                ceo = new Person();
                org.setCeo(ceo);
                break;
            default:
//                NOP
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "legal_entity":
                org.setLegalEntity(content);
                break;
            case "first_name":
                ceo.setFirstName(content);
                break;
            case "last_name":
                ceo.setLastName(content);
                break;
            case "patronymic":
                ceo.setPatronymic(content);
                break;
            case "authorized_capital":
                org.setAuthorizedCapital(content);
                break;
            case "org_parent":
                org.setParent(orgs.getOrganization(content));
                break;
            case "sh_last_name":
                shareholder.setLastName(content);
                break;
            case "sh_percentage":
                shareholder.setSharesPercentage(content);
                break;
            default:
//                NOP
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
}

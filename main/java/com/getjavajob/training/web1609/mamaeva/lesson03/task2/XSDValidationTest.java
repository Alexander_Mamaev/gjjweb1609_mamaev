package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import static com.getjavajob.training.web1609.mamaeva.lesson03.task2.XSDValidation.validate;
import static org.junit.Assert.assertEquals;

public class XSDValidationTest {
    private Source xsd;

    @Before
    public void setUp() {
        xsd = new StreamSource(getClass().getResourceAsStream("Organizations.xsd"));
    }

    @Test
    public void validateTest() throws Exception {
        Source xml = new StreamSource(getClass().getResourceAsStream("Organizations.xml"));
        assertEquals("validate test", true, validate(xsd, xml));
    }

    @Test
    public void validateTest1() throws Exception {
        Source xml = new StreamSource(getClass().getResourceAsStream("OrganizationsFailed1.xml"));
        assertEquals("validate test1", true, validate(xsd, xml));
    }

    @Test(expected = SAXException.class)
    public void validateTest2() throws Exception {
        Source xml = new StreamSource(getClass().getResourceAsStream("OrganizationsFailed2.xml"));
        validate(xsd, xml);
    }

    @Test(expected = SAXException.class)
    public void validateTest3() throws Exception {
        Source xml = new StreamSource(getClass().getResourceAsStream("OrganizationsFailed3.xml"));
        validate(xsd, xml);
    }
}
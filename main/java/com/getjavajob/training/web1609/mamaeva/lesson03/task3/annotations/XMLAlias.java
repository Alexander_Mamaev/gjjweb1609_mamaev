package com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface XMLAlias {
    String str();
}

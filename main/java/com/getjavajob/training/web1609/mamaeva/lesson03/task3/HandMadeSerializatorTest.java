package com.getjavajob.training.web1609.mamaeva.lesson03.task3;

import com.getjavajob.training.web1609.mamaeva.lesson03.task3.test.Country;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.test.Person;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.test.WorldParts;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.getjavajob.training.web1609.mamaeva.lesson03.task3.HandMadeSerializator.fromXML;
import static com.getjavajob.training.web1609.mamaeva.lesson03.task3.HandMadeSerializator.toXML;
import static org.junit.Assert.assertEquals;

public class HandMadeSerializatorTest {
    private Person person;

    @BeforeClass
    public static void init() throws IOException {
        File file = new File(HandMadeSerializator.class.getResource("person.xml").getPath());
        file.delete();
        file.createNewFile();
    }

    @Before
    public void setUp() {
        person = new Person();
        Country country = new Country();
        WorldParts worldParts = new WorldParts();

        person.setFirstName("Imya");
        person.setLastName("Familiya");
        person.setPatronymic("Otchestvo");
        person.setAge(30);
        person.setCountry(country);
        person.setExtraField2("extra2");

        country.setCountryName("Russia");
        country.setWorldParts(worldParts);

        worldParts.setName("ASIA");
        worldParts.setArea(30);
        worldParts.setNumberOfCountries(25);
        worldParts.setPopulation(23);
    }


    @Test
    public void toXMLTest() throws URISyntaxException, IOException {
        try (OutputStream os = new FileOutputStream(getClass().getResource("person.xml").getPath())) {
            toXML(person, os);
        } catch (IOException | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        byte[] expectedFile = Files.readAllBytes(Paths.get(getClass().getResource("test/personTest.xml").toURI()));
        byte[] actualFile = Files.readAllBytes(Paths.get(HandMadeSerializator.class.getResource("person.xml").toURI()));
        String expected = new String(expectedFile, StandardCharsets.UTF_8);
        String actual = new String(actualFile, StandardCharsets.UTF_8);
        assertEquals("toXMLTest", expected, actual);
        System.out.println(actual);
    }

    @Test
    public void fromXMLTest() {
        Class c = Person.class;
        Person newPerson = null;
        try (InputStream is = new FileInputStream(new File(getClass().getResource("test/personTest.xml").toURI()))) {
            newPerson = (Person) fromXML(is, c);
        } catch (IOException | InstantiationException | NoSuchFieldException | ClassNotFoundException | IllegalAccessException | URISyntaxException e) {
            e.printStackTrace();
        }
        assertEquals("fromXMLTest", true, person.equals(newPerson));
        System.out.println("-------------------");
        System.out.println(person);
        System.out.println(newPerson);
    }
}
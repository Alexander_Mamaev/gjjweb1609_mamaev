package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Organization;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Person;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Shareholder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class DOMParser {
    private OrganizationsList orgs;

    private static Shareholder parseShareholder(Node orgChildNode) {
        Shareholder shareholder = new Shareholder();
        NodeList shNodes = orgChildNode.getChildNodes();
        for (int k = 0; k < shNodes.getLength(); k++) {
            Node shNode = shNodes.item(k);
            if (shNode instanceof Element) {
                String content = shNode.getLastChild().getTextContent().trim();

                switch (shNode.getNodeName()) {
                    case "sh_last_name":
                        shareholder.setLastName(content);
                        break;
                    case "sh_percentage":
                        shareholder.setSharesPercentage(content);
                        break;
                    default:
                        throw new IllegalArgumentException("error in shareholder parsing");
                }
            }
        }
        return shareholder;
    }

    private static Person parseCEO(Node orgChildNode) {
        Person person = new Person();
        NodeList ceoNodes = orgChildNode.getChildNodes();
        for (int k = 0; k < ceoNodes.getLength(); k++) {
            Node ceoNode = ceoNodes.item(k);
            if (ceoNode instanceof Element) {
                String content = ceoNode.getLastChild().getTextContent().trim();

                switch (ceoNode.getNodeName()) {
                    case "first_name":
                        person.setFirstName(content);
                        break;
                    case "last_name":
                        person.setLastName(content);
                        break;
                    case "patronymic":
                        person.setPatronymic(content);
                        break;
                    default:
                        throw new IllegalArgumentException("error in ceo parsing");
                }
            }
        }
        return person;
    }

    public OrganizationsList parseOrganizations(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        // Get the DOM Builder Factory
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // Get the DOM Builder
        DocumentBuilder builder = factory.newDocumentBuilder();
        // Load and Parse the XML document
        // document contains the complete XML as a Tree
        Document document = builder.parse(is);

        orgs = new OrganizationsList();

        // Iterating through the nodes and extracting the data
        Element organizationsTag = document.getDocumentElement(); // <-- object of root tag
        // -- list of organizations --
        NodeList nodeList = organizationsTag.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element && node.getNodeName().equals("organization")) {
                Organization org = parseOrganization(node);
                orgs.add(org);
            }
        }
        return orgs;
    }

    private Organization parseOrganization(Node node) {
        Organization org = new Organization();
        org.setName(node.getAttributes().getNamedItem("name").getNodeValue());
        // -- list of fields of organization --
        NodeList orgChildNodes = node.getChildNodes();
        for (int j = 0; j < orgChildNodes.getLength(); j++) {
            Node orgChildNode = orgChildNodes.item(j);
            if (orgChildNode instanceof Element) {
                String content = orgChildNode.getLastChild().getTextContent().trim();
                switch (orgChildNode.getNodeName()) {
                    case "ceo":
                        org.setCeo(parseCEO(orgChildNode));
                        break;
                    case "legal_entity":
                        org.setLegalEntity(content);
                        break;
                    case "authorized_capital":
                        org.setAuthorizedCapital(content);
                        break;
                    case "org_parent":
                        org.setParent(orgs.getOrganization(content));
                        break;
                    case "shareholder":
                        org.addShareholder(parseShareholder(orgChildNode));
                        break;
                    default:
                        throw new IllegalArgumentException("default in swich case");
                }
            }
        }
        return org;
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.getjavajob.training.web1609.mamaeva.lesson03.task2.XSDValidation.validate;
import static com.getjavajob.training.web1609.mamaeva.lesson03.task2.XStreamOrgsListSerializator.orgsFromXML;
import static com.getjavajob.training.web1609.mamaeva.lesson03.task2.XStreamOrgsListSerializator.orgsToXML;
import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XStreamOrgsListSerializatorTest {
//    private OrganizationsList orgs;

//    @BeforeClass
//    public static void setOrgs() throws IOException, InterruptedException {
//        orgs = new OrganizationsList();
//        orgs.readFrom(new File(XStreamOrgsListSerializator.class.getResource("testFiles/fileWithLines2.txt").getPath()));
//    }

    @Test
    public void bSaveXML() throws InterruptedException {
        System.out.println("1");
        OrganizationsList orgs = new OrganizationsList();
        orgs.readFrom(new File(XStreamOrgsListSerializator.class.getResource("testFiles/fileWithLines2.txt").getPath()));

        try (OutputStream is = new FileOutputStream(new File(getClass().getResource("xStremSerializedOrgs.xml").getPath()))) {
            orgsToXML(orgs, is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Source xsd = new StreamSource(getClass().getResourceAsStream("Organizations.xsd"));
            Source xml = new StreamSource(getClass().getResourceAsStream("xStremSerializedOrgs.xml"));

            assertEquals("saveXML test", true, validate(xsd, xml));
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cReadAndSaveXML() throws IOException, InterruptedException, URISyntaxException {
        System.out.println("2");
        OrganizationsList orgs;
        orgs = orgsFromXML(new FileInputStream(new File(getClass().getResource("xStremSerializedOrgs.xml").getPath())));

        try (OutputStream os = new FileOutputStream(new File(getClass().getResource("xStremSerializedOrgs2.xml").getPath()))) {
            orgsToXML(orgs, os);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] file1Bytes = Files.readAllBytes(Paths.get(getClass().getResource("xStremSerializedOrgs.xml").toURI()));
        byte[] file2Bytes = Files.readAllBytes(Paths.get(getClass().getResource("xStremSerializedOrgs2.xml").toURI()));

        String file1 = new String(file1Bytes, StandardCharsets.UTF_8);
        String file2 = new String(file2Bytes, StandardCharsets.UTF_8);

        assertEquals("The content in the strings should match", file1, file2);
    }
}
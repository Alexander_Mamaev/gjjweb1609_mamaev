package com.getjavajob.training.web1609.mamaeva.lesson03.task2;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Organization;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Person;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Shareholder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.Map;

import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getBiggestOrgInfo;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getRichestShareholderInfo;

public class XStreamOrgsListSerializator {

    private static XStream getXStream() {
        XStream xStream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("$", "_")));
        xStream.alias("organizations", OrganizationsList.class);
        xStream.useAttributeFor(Organization.class, "name");
        xStream.aliasField("last_name", Person.class, "lastName");
        xStream.aliasField("first_name", Person.class, "firstName");
        xStream.alias("shareholder", Shareholder.class);
        xStream.aliasField("sh_last_name", Shareholder.class, "lastName");
        xStream.aliasField("sh_percentage", Shareholder.class, "sharesPercentage");

        xStream.addImplicitMap(OrganizationsList.class, "orgs", Map.class, "orgs");
        xStream.alias("organization", Organization.class);
        return xStream;
    }

    public static void orgsToXML(OrganizationsList orgs, OutputStream os) {
        XStream xStream = getXStream();
        xStream.registerConverter(new OrganizationConverter());
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"))) {
            bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            bw.write(xStream.toXML(orgs));
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static OrganizationsList orgsFromXML(InputStream is) {
        XStream xStream = getXStream();
        xStream.registerConverter(new OrganizationsListConverter());
        return (OrganizationsList) xStream.fromXML(is);
    }


    public static void main(String[] args) throws IOException, SAXException {
        OrganizationsList orgs = new OrganizationsList();
        orgs.readFrom(new File("src\\main\\java\\com\\getjavajob\\training\\web1609\\mamaeva\\lesson02\\testFiles\\fileWithLines2.txt"));

        System.out.println(getBiggestOrgInfo(orgs));
        System.out.println(getRichestShareholderInfo(orgs));

        String s1 = orgs.toString();
        orgsToXML(orgs, new FileOutputStream("src\\main\\java\\com\\getjavajob\\training\\web1609\\mamaeva\\lesson03\\task2\\xStremSerializedOrgs.xml"));
        orgs = orgsFromXML(new FileInputStream("src\\main\\java\\com\\getjavajob\\training\\web1609\\mamaeva\\lesson03\\task2\\xStremSerializedOrgs.xml"));
        orgsToXML(orgs, new FileOutputStream("src\\main\\java\\com\\getjavajob\\training\\web1609\\mamaeva\\lesson03\\task2\\xStremSerializedOrgs2.xml"));

        System.out.println(getBiggestOrgInfo(orgs));
        System.out.println(getRichestShareholderInfo(orgs));
        System.out.println(s1.equals(orgs.toString()));
//        System.out.println(orgs);
    }
}

class OrganizationConverter implements Converter {

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
        Organization org = (Organization) value;
        writer.addAttribute("name", org.getName());
        writer.startNode("legal_entity");
        context.convertAnother(org.getLegalEntity());
        writer.endNode();
        writer.startNode("ceo");
        context.convertAnother(org.getCeo());
        writer.endNode();
        writer.startNode("authorized_capital");
        context.convertAnother(org.getAuthorizedCapital());
        writer.endNode();
        writer.startNode("org_parent");
        if (org.getParent() == null) {
            writer.setValue("NONE");
        } else {
            context.convertAnother(org.getParent().getName());
        }
        writer.endNode();
        context.convertAnother(org.getShareholders());
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return null;
    }

    @Override
    public boolean canConvert(Class aClass) {
        return Organization.class.isAssignableFrom(aClass);
    }
}

class OrganizationsListConverter implements Converter {

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        OrganizationsList orgs = new OrganizationsList();

        while (reader.hasMoreChildren()) {
            Organization org = new Organization();
            reader.moveDown();
            org.setName(reader.getAttribute("name"));

            while (reader.hasMoreChildren()) {
                reader.moveDown();
                switch (reader.getNodeName()) {
                    case ("legal_entity"):
                        org.setLegalEntity(reader.getValue());
                        break;
                    case ("ceo"):
                        org.setCeo((Person) context.convertAnother(orgs, Person.class));
                        break;
                    case ("authorized_capital"):
                        org.setAuthorizedCapital(reader.getValue());
                        break;
                    case ("org_parent"):
                        org.setParent(orgs.getOrganization(reader.getValue()));
                        break;
                    case ("shareholder"):
                        org.addShareholder((Shareholder) context.convertAnother(orgs, Shareholder.class));
                        break;
                    default:
                        throw new IllegalArgumentException("default in switch case");
                }
                reader.moveUp();
            }
            reader.moveUp();
            orgs.add(org);
        }
        return orgs;
    }


    @Override
    public boolean canConvert(Class aClass) {
        return OrganizationsList.class.isAssignableFrom(aClass);
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson03.task3.test;

import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAlias;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAttribute;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLTransient;

@XMLAlias(str = "_person_")
public class Person {
    @XMLAttribute
    @XMLAlias(str = "_name_")
    private String firstName;
    @XMLAlias(str = "_last_name_")
    private String lastName;
    private Country country;
    @XMLAlias(str = "_patronymic_")
    private String patronymic;
    @XMLAlias(str = "_age_")
    private int age;
    @XMLTransient
    private String extraField1;
    @XMLAttribute
    private String extraField2;
    @XMLTransient
    private String extraField3;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setExtraField1(String extraField1) {
        this.extraField1 = extraField1;
    }

    public void setExtraField2(String extraField2) {
        this.extraField2 = extraField2;
    }

    public void setExtraField3(String extraField3) {
        this.extraField3 = extraField3;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country=" + country +
                ", patronymic='" + patronymic + '\'' +
                ", age=" + age +
                ", extraField1='" + extraField1 + '\'' +
                ", extraField2='" + extraField2 + '\'' +
                ", extraField3='" + extraField3 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        if (lastName != null ? !lastName.equals(person.lastName) : person.lastName != null) return false;
        if (country != null ? !country.equals(person.country) : person.country != null) return false;
        if (patronymic != null ? !patronymic.equals(person.patronymic) : person.patronymic != null) return false;
        if (extraField1 != null ? !extraField1.equals(person.extraField1) : person.extraField1 != null) return false;
        if (extraField2 != null ? !extraField2.equals(person.extraField2) : person.extraField2 != null) return false;
        return extraField3 != null ? extraField3.equals(person.extraField3) : person.extraField3 == null;

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + (extraField1 != null ? extraField1.hashCode() : 0);
        result = 31 * result + (extraField2 != null ? extraField2.hashCode() : 0);
        result = 31 * result + (extraField3 != null ? extraField3.hashCode() : 0);
        return result;
    }
}

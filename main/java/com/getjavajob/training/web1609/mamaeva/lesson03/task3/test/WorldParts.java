package com.getjavajob.training.web1609.mamaeva.lesson03.task3.test;

import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLAlias;
import com.getjavajob.training.web1609.mamaeva.lesson03.task3.annotations.XMLTransient;

@XMLAlias(str = "world_parts")
public class WorldParts {

    private String name;
    private double area; // millions km^2
    @XMLAlias(str = "_population_")
    private float population; // millions
    private long numberOfCountries;
    private boolean extraField1;
    @XMLTransient
    private byte extraField2;

    public void setName(String name) {
        this.name = name;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public void setNumberOfCountries(long numberOfCountries) {
        this.numberOfCountries = numberOfCountries;
    }

    @Override
    public String toString() {
        return "WorldParts{" +
                "name='" + name + '\'' +
                ", area=" + area +
                ", population=" + population +
                ", numberOfCountries=" + numberOfCountries +
                ", extraField1='" + extraField1 + '\'' +
                ", extraField2=" + extraField2 +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorldParts that = (WorldParts) o;

        if (Double.compare(that.area, area) != 0) return false;
        if (Float.compare(that.population, population) != 0) return false;
        if (numberOfCountries != that.numberOfCountries) return false;
        if (extraField1 != that.extraField1) return false;
        if (extraField2 != that.extraField2) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(area);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (population != +0.0f ? Float.floatToIntBits(population) : 0);
        result = 31 * result + (int) (numberOfCountries ^ (numberOfCountries >>> 32));
        result = 31 * result + (extraField1 ? 1 : 0);
        result = 31 * result + (int) extraField2;
        return result;
    }
}

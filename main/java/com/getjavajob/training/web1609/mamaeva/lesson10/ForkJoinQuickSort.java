package com.getjavajob.training.web1609.mamaeva.lesson10;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class ForkJoinQuickSort<T extends Comparable<T>> extends AbstractQuickSort<T> {
    private int threshold = 100_000;

    @Override
    public void sort(Collection<T> col) {
        List<T> list = (List<T>) col;
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(new ForkJoinThread(list));
        pool.shutdown();
    }

    @Override
    public void sort(Collection<T> col, int threshold) {
        this.threshold = threshold;
        sort(col);
    }

    private class ForkJoinThread extends RecursiveAction {
        private List<T> list;
        private int left;
        private int right;

        public ForkJoinThread(List<T> list) {
            this.list = list;
            left = 0;
            right = list.size() - 1;
        }

        private ForkJoinThread(List<T> list, int left, int right) {
            this.list = list;
            this.left = left;
            this.right = right;
        }

        @Override
        protected void compute() {
            if (right - left < threshold) {
                SingleQuickSort<T> qs = new SingleQuickSort<>();
                qs.sort(list, left, right);
            } else {
//                if (left < right) {
                int pivot = partition(list, left, right);
                invokeAll(new ForkJoinThread(list, left, pivot),
                        new ForkJoinThread(list, pivot + 1, right));
            }
        }
    }
}

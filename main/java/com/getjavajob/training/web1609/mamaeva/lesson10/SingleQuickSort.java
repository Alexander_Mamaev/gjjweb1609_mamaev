package com.getjavajob.training.web1609.mamaeva.lesson10;

import java.util.Collection;
import java.util.List;

public class SingleQuickSort<T extends Comparable<T>> extends AbstractQuickSort<T> {

    @Override
    public void sort(Collection<T> col) {
        List<T> list = (List<T>) col;
        sort(list, 0, list.size() - 1);
    }

    @Override
    public void sort(Collection<T> col, int threshold) {
        sort(col);
    }

    protected void sort(List<T> list, int left, int right) {
        if (left < right) {
            int pivot = partition(list, left, right);
            sort(list, left, pivot);
            sort(list, pivot + 1, right);
        }
    }
}

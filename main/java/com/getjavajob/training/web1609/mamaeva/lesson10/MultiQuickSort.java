package com.getjavajob.training.web1609.mamaeva.lesson10;

import java.util.Collection;
import java.util.List;

public class MultiQuickSort<T extends Comparable<T>> extends AbstractQuickSort<T> {
    private int threshold = 100_000;

    @Override
    public void sort(Collection<T> col) {
        List<T> list = (List<T>) col;
        new SortingThread(list, 0, list.size() - 1).run();
    }

    @Override
    public void sort(Collection<T> col, int threshold) {
        this.threshold = threshold;
        sort(col);
    }

    private class SortingThread extends Thread {
        private List<T> list;
        private int left;
        private int right;

        public SortingThread(List<T> list, int left, int right) {
            this.list = list;
            this.left = left;
            this.right = right;
        }

        @Override
        public void run() {
            if (right - left < threshold) {
                SingleQuickSort<T> qs = new SingleQuickSort<>();
                qs.sort(list, left, right);
            } else {
//            if (left < right) {
                int pivot = partition(list, left, right);
                Thread th1 = new SortingThread(list, left, pivot);
                Thread th2 = new SortingThread(list, pivot + 1, right);
                th1.start();
                th2.start();

                try {
                    th1.join();
                    th2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

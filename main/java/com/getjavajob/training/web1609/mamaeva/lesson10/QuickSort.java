package com.getjavajob.training.web1609.mamaeva.lesson10;

import java.util.Collection;

public interface QuickSort<T extends Comparable<T>> {
    void sort(Collection<T> col);
    void sort(Collection<T> col, int threshold);
}

package com.getjavajob.training.web1609.mamaeva.lesson10;

import com.getjavajob.training.web1609.mamaeva.util.StopWatch;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SortsTest {
    private static final int SIZE = 100_000_000;
    private static StopWatch time;
    private Collection<Integer> intCol;
    private String intExpected;
    private Collection<String> strCol;
    private String strExpected;
    private Collection<Integer> randomCol;

    @BeforeClass
    public static void start() {
        System.out.println("SIZE = " + SIZE);
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    @Before
    public void setUp() {
        System.gc();
        time = new StopWatch();
        intCol = new ArrayList<>(Arrays.asList(1, 5, 2, 4, 3, 0, 9, 6, 7, 8));
        intExpected = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]";
        strCol = new ArrayList<>(Arrays.asList("ac", "aa", "ab", "ba", "bc", "bb"));
        strExpected = "[aa, ab, ac, ba, bb, bc]";
    }

    @Test
    public void intSingleQuickSortTest() {
        QuickSort<Integer> quickSort = new SingleQuickSort<>();
        quickSort.sort(intCol);
        assertEquals(intExpected, intCol.toString());
    }

    @Test
    public void strSingleQuickSortTest() {
        QuickSort<String> quickSort = new SingleQuickSort<>();
        quickSort.sort(strCol);
        assertEquals(strExpected, strCol.toString());
    }

    @Test
    public void randomIntSingleQuickSortTest() {
        QuickSort<Integer> quickSort = new SingleQuickSort<>();
        fillRandomCol(SIZE);
        time.start();
        quickSort.sort(randomCol, 100_000);
        System.out.println("randomIntSingleQuickSortTest(SIZE): " + time.getElapsedTime());
        assertEquals("", true, verifySortedCollection(randomCol));
    }

    @Test
    public void intMultiQuickSortTest() {
        QuickSort<Integer> quickSort = new MultiQuickSort<>();
        quickSort.sort(intCol);
        assertEquals(intExpected, intCol.toString());
    }

    @Test
    public void strMultiQuickSortTest() {
        QuickSort<String> quickSort = new MultiQuickSort<>();
        quickSort.sort(strCol);
        assertEquals(strExpected, strCol.toString());
    }

    @Test
    public void randomIntMultiQuickSortTest() {
        QuickSort<Integer> quickSort = new MultiQuickSort<>();
        fillRandomCol(SIZE);
        time.start();
        quickSort.sort(randomCol, 1_000_000);
        System.out.println("randomIntMultiQuickSortTest(SIZE): " + time.getElapsedTime());
        assertEquals(true, verifySortedCollection(randomCol));
    }

    @Test
    public void intForkJoinQuickSortTest() {
        QuickSort<Integer> quickSort = new ForkJoinQuickSort<>();
        quickSort.sort(intCol);
        assertEquals(intExpected, intCol.toString());
    }

    @Test
    public void strForkJoinQuickSortTest() {
        QuickSort<String> quickSort = new ForkJoinQuickSort<>();
        quickSort.sort(strCol);
        assertEquals(strExpected, strCol.toString());
    }

    @Test
    public void randomIntForkJoinQuickSortTest() {
        QuickSort<Integer> quickSort = new ForkJoinQuickSort<>();
        fillRandomCol(SIZE);
        time.start();
        quickSort.sort(randomCol, 100_000);
        System.out.println("randomIntForkJoinQuickSortTest(SIZE): " + time.getElapsedTime());
        assertEquals(true, verifySortedCollection(randomCol));
    }

    private <T extends Comparable<T>> boolean verifySortedCollection(Collection<T> col) {
        List<T> list = (List<T>) col;
        for (int i = 1; i < col.size(); i++) {
            if (list.get(i).compareTo(list.get(i - 1)) < 0) {
                return false;
            }
        }
        return true;
    }

    private void fillRandomCol(int size) {
        randomCol = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            randomCol.add((int) (Math.random() * 100));
        }
    }
}
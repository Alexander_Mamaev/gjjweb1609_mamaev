package com.getjavajob.training.web1609.mamaeva.lesson10;

import java.util.List;

public abstract class AbstractQuickSort<T extends Comparable<T>> implements QuickSort<T> {

    protected int partition(List<T> list, int left, int right) {
        T pivot = list.get(left + (right - left) / 2);
        int l = left - 1;
        int r = right + 1;
        while (true) {
            do {
                l++;
            }
            while (list.get(l).compareTo(pivot) < 0);

            do {
                r--;
            }
            while (list.get(r).compareTo(pivot) > 0);
            if (l >= r)
                return r;

            swap(list, l, r);
        }
    }

    private void swap(List<T> list, int x, int y) {
        T temp = list.get(x);
        list.set(x, list.get(y));
        list.set(y, temp);
    }
}

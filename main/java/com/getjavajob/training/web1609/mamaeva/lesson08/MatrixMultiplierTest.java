package com.getjavajob.training.web1609.mamaeva.lesson08;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;


public class MatrixMultiplierTest {
    private static InputStream is1;
    private static InputStream is2;
    private static InputStream is3;
    private static OutputStream os;
    private static String expected;

    @Before
    public void setUp() throws IOException, URISyntaxException {
        is1 = new FileInputStream(MatrixMultiplierTest.class.getResource("testFiles/matrix1.txt").getPath());
        is2 = new FileInputStream(MatrixMultiplierTest.class.getResource("testFiles/matrix2.txt").getPath());
        is3 = new FileInputStream(MatrixMultiplierTest.class.getResource("testFiles/matrix2illegal.txt").getPath());
        os = new FileOutputStream(new File(MatrixMultiplierTest.class.getResource("testFiles/result.txt").getPath()));

        byte[] expectedFile = Files.readAllBytes(Paths.get(getClass().getResource("testFiles/expectedResult.txt").toURI()));
        expected = new String(expectedFile, StandardCharsets.UTF_8);
    }

    @Test
    public void matrixMultiplierSingleTest() throws URISyntaxException, IOException {
        MatrixMultiplier ms = new MatrixMultiplierSingle();
        ms.multiply(is1, is2, os);

        byte[] actualFile = Files.readAllBytes(Paths.get(getClass().getResource("testFiles/result.txt").toURI()));
        String actual = new String(actualFile, StandardCharsets.UTF_8);

        assertEquals("matrixMultiplierSingleTest", expected, actual);
    }

    @Test
    public void matrixMultiplierMTTest() throws URISyntaxException, IOException {
        MatrixMultiplier mt = new MatrixMultiplierMT();
        mt.multiply(is1, is2, os);

        byte[] actualFile = Files.readAllBytes(Paths.get(getClass().getResource("testFiles/result.txt").toURI()));
        String actual = new String(actualFile, StandardCharsets.UTF_8);

        assertEquals("matrixMultiplierMTTest", expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalMatrixMultiplierTest() throws URISyntaxException, IOException {
        MatrixMultiplier mms = new MatrixMultiplierMT();
        mms.multiply(is1, is3, os);
    }
}
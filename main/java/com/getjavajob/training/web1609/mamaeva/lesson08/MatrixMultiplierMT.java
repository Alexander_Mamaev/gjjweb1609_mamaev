package com.getjavajob.training.web1609.mamaeva.lesson08;

import java.util.ArrayList;
import java.util.List;

public class MatrixMultiplierMT extends AbstractMatrixMultiplier {

    @Override
    public int[][] multiply(int[][] matrix1, int[][] matrix2) {
        int n = matrix1.length;
        int k = matrix2[0].length;
        int[][] resultedMatrix = new int[n][k];
        List<Thread> thr = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            Thread thread = new MatrixMultiplicationThread(i, resultedMatrix, matrix1, matrix2);
            thr.add(thread);
            thread.start();
        }

        for (Thread t : thr) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return resultedMatrix;
    }

    private static class MatrixMultiplicationThread extends Thread {
        private int i; // row number of resulted matrix
        private int[][] resultedMatrix;
        private int[][] matrix1;
        private int[][] matrix2;

        public MatrixMultiplicationThread(int i, int[][] resultedMatrix, int[][] matrix1, int[][] matrix2) {
            this.i = i;
            this.resultedMatrix = resultedMatrix;
            this.matrix1 = matrix1;
            this.matrix2 = matrix2;
        }

        @Override
        public void run() {
            int k = matrix2[0].length;
            int m = matrix2.length;

            for (int j = 0; j < k; j++) {
                for (int p = 0; p < m; p++) {
                    resultedMatrix[i][j] += matrix1[i][p] * matrix2[p][j];
                }
            }
        }
    }
}

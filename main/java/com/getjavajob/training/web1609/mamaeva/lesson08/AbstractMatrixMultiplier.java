package com.getjavajob.training.web1609.mamaeva.lesson08;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

abstract public class AbstractMatrixMultiplier implements MatrixMultiplier {
    @Override
    public void multiply(InputStream is1, InputStream is2, OutputStream os) {
        int[][] matrix1 = readMatrix(is1);
        int[][] matrix2 = readMatrix(is2);
        if (validate(matrix1, matrix2)) {
            int[][] result = multiply(matrix1, matrix2);
            saveMatrix(result, os);
        }
    }

    @Override
    public abstract int[][] multiply(int[][] matrix1, int[][] matrix2);

    private static boolean validate(int[][] matrix1, int[][] matrix2) {
        if (matrix1[0].length != matrix2.length) {
            throw new IllegalArgumentException("Matrix can't be multiplied (check matrix size: [NxM]x[MxK])");
        }
        return true;
    }

    private static int[][] readMatrix(InputStream is) {
        List<String[]> matrix = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            while (br.ready()) {
                String[] line = br.readLine().split(" ");
                if (line.length > 0) {
                    matrix.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parseMatrix(matrix);
    }

    private static int[][] parseMatrix(List<String[]> matrix) {
        int[][] matrixInt;
        try {
            int matrixRawSize = matrix.get(0).length;
            matrixInt = new int[matrix.size()][matrixRawSize];
            for (int i = 0; i < matrix.size(); i++) {
                String[] line = matrix.get(i);
                if (line.length == matrixRawSize) {
                    for (int j = 0; j < line.length; j++) {
                        matrixInt[i][j] = Integer.parseInt(line[j]);
                    }
                } else {
                    throw new IllegalArgumentException("wrong matrix input format (row length must not be variable)");
                }
            }
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("wrong matrix input format", e);
        }
        return matrixInt;
    }

    private static String matrixToString(int[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int[] aMatrix : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                sb.append(aMatrix[j]).append(" ");
            }
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    private static void saveMatrix(int[][] matrix, OutputStream os) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            bw.write(matrixToString(matrix));
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

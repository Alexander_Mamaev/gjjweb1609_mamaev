package com.getjavajob.training.web1609.mamaeva.lesson08;

import java.io.InputStream;
import java.io.OutputStream;

public interface MatrixMultiplier {

    void multiply(InputStream is1, InputStream is2, OutputStream os);

    int[][] multiply(int[][] matrix1, int[][] matrix2);
}

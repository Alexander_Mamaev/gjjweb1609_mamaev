package com.getjavajob.training.web1609.mamaeva.lesson08;

public class MatrixMultiplierSingle extends AbstractMatrixMultiplier {

    @Override
    public int[][] multiply(int[][] matrix1, int[][] matrix2) {
        int n = matrix1.length;
        int k = matrix2[0].length;
        int m = matrix2.length;
        int[][] resultedMatrix = new int[n][k];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                for (int p = 0; p < m; p++) {
                    resultedMatrix[i][j] += matrix1[i][p] * matrix2[p][j];
                }
            }
        }
        return resultedMatrix;
    }
}

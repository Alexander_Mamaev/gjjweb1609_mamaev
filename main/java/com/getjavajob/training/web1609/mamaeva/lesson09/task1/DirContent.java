package com.getjavajob.training.web1609.mamaeva.lesson09.task1;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import static com.getjavajob.training.web1609.mamaeva.util.ConsoleReader.readString;

public class DirContent {
    public static void main(String[] args) throws IOException {
        System.out.println("Please enter the dir path.");
        String path = readString();
        System.out.println("Please enter the file extension.");
        String ext = readString();
        System.out.println(dirContent(path, ext));
    }

    public static String dirContent(String path, String ext) throws IOException {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        File dir = new File(path);
        sb.append("--------------------------------").append(System.lineSeparator());
        sb.append("Result of searching *.").append(ext).append(" files in ").append(dir.toPath().toRealPath()).append(System.lineSeparator());
        sb.append("--------------------------------").append(System.lineSeparator());
        if (validateDirPath(dir)) {
            getDirContent(sb, dir, ext, sdf);
            return sb.toString();
        }
        return null;
    }

    private static boolean validateDirPath(File file) {
        if (!file.exists() || !file.isDirectory()) {
            throw new IllegalArgumentException("incorrect dir path!");
        }
        return true;
    }

    private static void getDirContent(StringBuilder sb, File dir, String ext, SimpleDateFormat sdf) throws IOException {
        boolean flag = true;
        File[] files = dir.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isFile()) {
                    String[] name = f.getName().split("\\.");
                    if ((name.length > 1 && name[name.length - 1].equals(ext)) || ext.equals("")) {
                        if (flag) {
                            appendDirInfo(sb, dir, sdf);
                            flag = false;
                        }
                        appendFileInfo(sb, f, sdf);
                    }
                } else {
                    getDirContent(sb, f, ext, sdf);
                }
            }
        }
    }

    private static void appendFileInfo(StringBuilder sb, File f, SimpleDateFormat sdf) throws IOException {
        sb.append("  ").append(rwx(f)).append(" ").append(totalSpace(f)).append(" byte ").
                append(sdf.format(f.lastModified())).append("    ").append(f.toPath().toRealPath()).append(System.lineSeparator());
    }

    private static void appendDirInfo(StringBuilder sb, File dir, SimpleDateFormat sdf) throws IOException {
        sb.append("d ").append(rwx(dir)).append(" ").append(sdf.format(dir.lastModified())).
                append("    ").append(dir.toPath().toRealPath()).append(System.lineSeparator());
    }

    private static long totalSpace(File f) {
        return f.length();
    }

    private static StringBuilder rwx(File f) {
        StringBuilder sb = new StringBuilder();
        if (f.canRead()) {
            sb.append("r");
        }
        if (f.canWrite()) {
            sb.append("w");
        }
        if (f.canExecute()) {
            sb.append("x");
        }
        return sb;
    }
}
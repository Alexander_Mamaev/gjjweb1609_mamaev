package com.getjavajob.training.web1609.mamaeva.lesson09.task3;

import java.util.concurrent.Semaphore;

import static com.getjavajob.training.web1609.mamaeva.util.Utils.someSleep;

public class ProducerConsumerSemaphore {
    private int number;
    private Semaphore semaphoreProducer;
    private Semaphore semaphoreEvenConsumer;
    private Semaphore semaphoreOddConsumer;

    public static void main(String[] args) {
        ProducerConsumerSemaphore pc = new ProducerConsumerSemaphore();
        pc.runner();
    }

    private void runner() {
        semaphoreProducer = new Semaphore(1);
        semaphoreEvenConsumer = new Semaphore(0);
        semaphoreOddConsumer = new Semaphore(0);

        new Producer().start();
        new Thread(new Consumer(semaphoreEvenConsumer), "EvenConsumer").start();
        new Thread(new Consumer(semaphoreOddConsumer), "OddConsumer").start();
    }

    private class Producer extends Thread {

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    semaphoreProducer.acquire();
                    number = (int) (Math.random() * 10);
                    System.out.println("Producer make this: " + number);
                    if (number % 2 == 0) {
                        semaphoreEvenConsumer.release();
                    } else {
                        semaphoreOddConsumer.release();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                someSleep(1000);
            }
        }
    }

    private class Consumer implements Runnable {
        private Semaphore semaphore;

        public Consumer(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + " eat this: " + number);
                    semaphoreProducer.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                someSleep(1500);
            }
        }
    }
}

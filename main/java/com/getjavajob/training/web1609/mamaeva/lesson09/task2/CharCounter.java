package com.getjavajob.training.web1609.mamaeva.lesson09.task2;

import java.io.*;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CharCounter {
    private CountDownLatch countDownLatch;
    private Map<Character, Integer> map;
    private StringBuilder sb;
    private static String russianCharset = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    public SortedSet<Map.Entry<Character, Integer>> countChars(File file) {
        map = new ConcurrentHashMap<>();
        sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            while (br.ready()) {
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        parallelCharCount();
        return reversedValuesSortedSet(map);
    }

    private void parallelCharCount() {
        int size = russianCharset.length();
        countDownLatch = new CountDownLatch(size);
        ExecutorService threadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < size; i++) {
            threadPool.execute(new CountingThread(russianCharset.charAt(i)));
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        threadPool.shutdown();
    }

    private SortedSet<Map.Entry<Character, Integer>> reversedValuesSortedSet(Map<Character, Integer> map) {
        SortedSet<Map.Entry<Character, Integer>> set = new TreeSet<>((o1, o2) -> {
            int res = o2.getValue().compareTo(o1.getValue());
            return res != 0 ? res : 1;
        });
        set.addAll(map.entrySet());
        return set;
    }

    private class CountingThread extends Thread {
        private char letter;
        private int counter;

        public CountingThread(char letter) {
            this.letter = letter;
        }

        @Override
        public void run() {
            for (int i = 0; i < sb.length(); i++) {
                if (Character.toLowerCase(sb.charAt(i)) == letter) {
                    counter++;
                }
            }
            if (counter > 0) {
                map.put(letter, counter);
            }
            countDownLatch.countDown();
        }
    }
}
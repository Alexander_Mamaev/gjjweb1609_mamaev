package com.getjavajob.training.web1609.mamaeva.lesson09.task3;

import java.util.concurrent.Exchanger;

import static com.getjavajob.training.web1609.mamaeva.util.Utils.someSleep;

public class ProducerConsumerExchanger {
    private Exchanger<Integer> exchangerOdd;
    private Exchanger<Integer> exchangerEven;

    public static void main(String[] args) {
        ProducerConsumerExchanger pc = new ProducerConsumerExchanger();
        pc.runner();
    }

    private void runner() {
        exchangerOdd = new Exchanger<>();
        exchangerEven = new Exchanger<>();

        new Producer().start();
        new Thread(new Consumer(exchangerEven), "EvenConsumer").start();
        new Thread(new Consumer(exchangerOdd), "OddConsumer").start();
    }

    private class Producer extends Thread {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    int number = (int) (Math.random() * 10);
                    System.out.println("Producer make this: " + number);
                    if (number % 2 == 0) {
                        exchangerEven.exchange(number);
                    } else {
                        exchangerOdd.exchange(number);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class Consumer implements Runnable {
        private Exchanger<Integer> exchanger;

        public Consumer(Exchanger<Integer> exchanger) {
            this.exchanger = exchanger;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    someSleep(800);
                    System.out.println(Thread.currentThread().getName() + " eat this: " + exchanger.exchange(0));
                    someSleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

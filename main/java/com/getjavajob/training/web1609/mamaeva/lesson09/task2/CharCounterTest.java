package com.getjavajob.training.web1609.mamaeva.lesson09.task2;

import org.junit.Test;

import java.io.File;
import java.util.SortedSet;

import static org.junit.Assert.assertEquals;

public class CharCounterTest {

    @Test
    public void countCharsTest2() throws Exception {
        File file = new File(getClass().getResource("simpleText.txt").getPath());
        String expected = "[а=100, б=99, в=98, г=97]";

        for (int i = 0; i < 1000; i++) {
            SortedSet set = new CharCounter().countChars(file);
            assertEquals(expected, set.toString());
        }
    }

    @Test
    public void countCharsTest3() throws Exception {
        File file = new File(getClass().getResource("Text.txt").getPath());
        String expected = "[о=265, и=194, е=182, н=182, а=167, т=147, с=142, р=116, в=106, л=81, к=78, д=72, м=67, п=67, у=59, ы=50, я=41, г=40, з=37, ь=35, х=34, й=32, б=30, ч=22, ц=21, ш=21, ю=12, ж=11, щ=10, ф=5, э=4, ъ=2, ё=1]";

        for (int i = 0; i < 1000; i++) {
            SortedSet set = new CharCounter().countChars(file);
            assertEquals(expected, set.toString());
        }
    }
}
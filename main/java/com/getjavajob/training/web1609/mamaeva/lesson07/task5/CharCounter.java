package com.getjavajob.training.web1609.mamaeva.lesson07.task5;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class CharCounter {
    private Map<Character, Integer> map;
    private StringBuilder sb;
    private Collection<Thread> threads;
    private String russianCharset = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    public SortedSet<Map.Entry<Character, Integer>> countChars(File file) throws InterruptedException {
        map = new HashMap<>();
        sb = new StringBuilder();
        threads = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "windows-1251"))) {
            while (br.ready()) {
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        parallelCharCount();
        waitCounting();
        return reversedValuesSortedSet(map);
    }

    private void parallelCharCount() {
        for (int i = 0; i < russianCharset.length(); i++) {
            Thread thread = new CountingThread(russianCharset.charAt(i));
            threads.add(thread);
            thread.start();
        }
    }

    private void waitCounting() {
        for (Thread thr : threads) {
            try {
                thr.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private SortedSet<Map.Entry<Character, Integer>> reversedValuesSortedSet(Map<Character, Integer> map) {
        SortedSet<Map.Entry<Character, Integer>> set = new TreeSet<>((o1, o2) -> {
            int res = o2.getValue().compareTo(o1.getValue());
            return res != 0 ? res : 1;
        });
        set.addAll(map.entrySet());
        return set;
    }

    private class CountingThread extends Thread {
        private char letter;
        private int counter;

        public CountingThread(char letter) {
            this.letter = letter;
        }

        @Override
        public void run() {
            for (int i = 0; i < sb.length(); i++) {
                if (Character.toLowerCase(sb.charAt(i)) == letter) {
                    counter++;
                }
            }
            if (counter > 0) {
                putToMap(letter, counter);
            }
        }
    }

    private synchronized void putToMap(char letter, int count) {
        map.put(letter, count);
    }
}


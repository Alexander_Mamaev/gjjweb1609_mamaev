package com.getjavajob.training.web1609.mamaeva.lesson02.validators;

public class ShareholderInformationException extends RuntimeException {
    public ShareholderInformationException(String message) {
        super(message);
    }

    public ShareholderInformationException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson02.testFiles;

import com.getjavajob.training.web1609.mamaeva.lesson02.Runner;
import com.getjavajob.training.web1609.mamaeva.lesson02.validators.AuthorizedCapitalException;
import com.getjavajob.training.web1609.mamaeva.lesson02.validators.InputShareholderInformationException;
import com.getjavajob.training.web1609.mamaeva.lesson02.validators.LegalEntityException;
import com.getjavajob.training.web1609.mamaeva.lesson02.validators.ShareholderInformationException;
import org.junit.Test;

public class ExceptionsTest {

    @Test(expected = AuthorizedCapitalException.class)
    public void authorizedCapitalExceptionTest1() {
        String[] txt = new String[]{getClass().getResource("authorizedCapitalException1.txt").getPath()};
        Runner.main(txt);

    }

    @Test(expected = AuthorizedCapitalException.class)
    public void authorizedCapitalExceptionTest2() {
        String[] txt = new String[]{getClass().getResource("authorizedCapitalException2.txt").getPath()};
        Runner.main(txt);
    }

    @Test(expected = InputShareholderInformationException.class)
    public void InputShareholderInformationTest() {
        String[] txt = new String[]{getClass().getResource("inputShareholderInformationException.txt").getPath()};
        Runner.main(txt);

    }

    @Test(expected = LegalEntityException.class)
    public void legalEntityExceptionTest() {
        String[] txt = new String[]{getClass().getResource("legalEntityException.txt").getPath()};
        Runner.main(txt);
    }

    @Test(expected = ShareholderInformationException.class)
    public void ShareholderInformationExceptionTest1() {
        String[] txt = new String[]{getClass().getResource("shareholderInformationException1.txt").getPath()};
        Runner.main(txt);
    }

    @Test(expected = ShareholderInformationException.class)
    public void ShareholderInformationExceptionTest2() {
        String[] txt = new String[]{getClass().getResource("shareholderInformationException2.txt").getPath()};
        Runner.main(txt);
    }

    @Test(expected = ShareholderInformationException.class)
    public void ShareholderInformationExceptionTest3() {
        String[] txt = new String[]{getClass().getResource("shareholderInformationException3.txt").getPath()};
        Runner.main(txt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nameValidationFailedTest() {
        String[] txt = new String[]{getClass().getResource("nameValidationFailed.txt").getPath()};
        Runner.main(txt);
    }
}

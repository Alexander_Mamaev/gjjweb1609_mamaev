package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.io.Serializable;

import static com.getjavajob.training.web1609.mamaeva.lesson02.validators.Validators.nameValidator;

/**
 * describes information about people
 */
public class Person implements Serializable {

    private static final long serialVersionUID = 20161002;

    private String firstName;
    private String lastName;
    private String patronymic;

    public Person() {
    }

    public Person(String lastName, String firstName, String patronymic) {
        this.lastName = nameValidator(lastName);
        this.firstName = nameValidator(firstName);
        this.patronymic = nameValidator(patronymic);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = nameValidator(lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = nameValidator(firstName);
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = nameValidator(patronymic);
    }

    @Override
    public String toString() {
        return "Person{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import com.getjavajob.training.web1609.mamaeva.lesson02.validators.ShareholderInformationException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import static com.getjavajob.training.web1609.mamaeva.lesson02.validators.Validators.*;

public class Organization implements Serializable {

    private static final long serialVersionUID = 20161002;

    private String name;
    private LegalEntity legalEntity;
    private Person ceo;
    private double authorizedCapital;
    private Organization parent;
    private Collection<Shareholder> shareholders;

    public Organization() {
        shareholders = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = nameValidator(name);
    }

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = entityValidator(legalEntity);
    }

    public Person getCeo() {
        return ceo;
    }

    public void setCeo(Person ceo) {
        this.ceo = ceo;
    }

    public double getAuthorizedCapital() {
        return authorizedCapital;
    }

    public void setAuthorizedCapital(String authorizedCapital) {
        this.authorizedCapital = authorizedCapitalValidator(authorizedCapital);
    }

    public Organization getParent() {
        return parent;
    }

    public void setParent(Organization parent) {
        this.parent = parent;
    }

    public Collection<Shareholder> getShareholders() {
        return shareholders;
    }

    public void addShareholder(Shareholder shareholder) {
        shareholders.add(shareholder);
        if (!collectivePercentageValidator(shareholders)) {
            throw new ShareholderInformationException("Overall percentage of shares NOT equals 0 - 100%");
        }
    }

    @Override
    public String toString() {
        String parentName;
        if (parent != null) {
            parentName = parent.getName();
        } else {
            parentName = "NONE";
        }

        return "Organization{" + System.lineSeparator() +
                "name='" + name + '\'' + System.lineSeparator() +
                ", legalEntity=" + legalEntity + System.lineSeparator() +
                ", ceo=" + ceo + System.lineSeparator() +
                ", authorizedCapital=" + authorizedCapital + System.lineSeparator() +
                ", parent=" + parentName + System.lineSeparator() +
                ", shareholders=" + shareholders + System.lineSeparator() +
                '}' + System.lineSeparator();
    }
}

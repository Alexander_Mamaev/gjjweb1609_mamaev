package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.io.Serializable;

public enum LegalEntity implements Serializable {
    OOO, OAO, ZAO, IP
}

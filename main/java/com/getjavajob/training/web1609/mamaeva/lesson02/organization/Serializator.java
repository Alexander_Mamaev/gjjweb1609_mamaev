package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.io.*;

public class Serializator {
    public static OrganizationsList deserialization(File file) {
        OrganizationsList orgs = null;
        try (ObjectInputStream objISTrm = new ObjectInputStream(new FileInputStream(file))) {
            orgs = (OrganizationsList) objISTrm.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return orgs;
    }

    public static void serialization(OrganizationsList orgs, File file) {
        try (ObjectOutputStream objOStrm = new ObjectOutputStream(new FileOutputStream(file))) {
            objOStrm.writeObject(orgs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

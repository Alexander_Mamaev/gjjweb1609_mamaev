package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.web1609.mamaeva.lesson02.validators.Validators.inputValidator;

public class OrganizationsList implements Serializable {

    private static final long serialVersionUID = 20161002;

    private Map<String, Organization> orgs;

    public OrganizationsList() {
        orgs = new LinkedHashMap<>();
    }

    public OrganizationsList(Map<String, Organization> orgs) {
        this.orgs = orgs;
    }

    public void add(Organization org) {
        orgs.put(org.getName(), org);
    }

    public Map<String, Organization> getOrgs() {
        return orgs;
    }

    public void setOrgs(Map<String, Organization> orgs) {
        this.orgs = orgs;
    }

    public Map<String, Organization> asMap() {
        return orgs;
    }

    public Organization getOrganization(String name) {
        if (name.equals("NONE")) {
            return null;
        }
        return orgs.get(name);
    }

    public void readFrom(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                readFrom(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFrom(String line) {
        Organization org = new Organization();
        String[] fields = inputValidator(line.split(" "));

        org.setName(fields[0]);
        org.setLegalEntity(fields[1]);
        org.setCeo(new Person(fields[2], fields[3], fields[4]));
        org.setAuthorizedCapital(fields[5]);
        org.setParent(getOrganization(fields[6]));
        for (int i = 7; i < fields.length; i++) {
            Shareholder shareholder = new Shareholder(fields[i], fields[++i]);
            org.addShareholder(shareholder);
        }
        orgs.put(fields[0], org);
    }


    @Override
    public String toString() {
        return "OrganizationsList{" +
                "orgs=" + orgs +
                '}';
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson02.testFiles;

import com.getjavajob.training.web1609.mamaeva.lesson02.Runner;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class IOTest {
    private String[] param;
    private String expected;

    public IOTest(String[] param, String expected) {
        this.param = param;
        this.expected = expected;
    }

    @BeforeClass
    public static void setFields() throws IOException {
        File serial = new File(Runner.class.getResource("Organizations.dat").getPath());
        serial.delete();
        serial.createNewFile();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParameter() {
        String expectedResult1 = "Please enter the organization information (enter the empty line to input)" + System.lineSeparator() +
                "LastName3" + System.lineSeparator() + "Organization1" + System.lineSeparator();
        String expectedResult2 = "aLastName1" + System.lineSeparator() + "Organization1" + System.lineSeparator();
        String expectedResult3 = "aLastName1" + System.lineSeparator() + "Organization3" + System.lineSeparator();
        String expectedResult4 = "aLastName2" + System.lineSeparator() + "Organization3" + System.lineSeparator();

        String txt1 = IOTest.class.getResource("fileWithLines1.txt").getPath();
        String txt2 = IOTest.class.getResource("fileWithLines2.txt").getPath();
        String txt3 = IOTest.class.getResource("fileWithLines3.txt").getPath();

        return Arrays.asList(new Object[][]{
                {new String[]{}, expectedResult1},
                {new String[]{txt1}, expectedResult2},
                {new String[]{txt2}, expectedResult3},
                {new String[]{txt3}, expectedResult4}
        });
    }

    @Test
    public void runnerTest() {
        InputStream sysIn = System.in;
        PrintStream sysOut = System.out;

        String data = "Organization1 OOO LasName1 Name1 Patronymic1 10.0 NONE aLastName1 100" + System.lineSeparator() +
                "Organization2 OAO LasName2 Name2 Patronymic2 20 NONE aLastName2 50 LastName3 50" + System.lineSeparator() +
                "Organization3 ZAO LasName3 Name3 Patronymic3 30 Organization1 aLastName1 50 LastName4 25 aLastName5 25" + System.lineSeparator() +
                "Organization4 IP LasName4 Name4 Patronymic4 40.5 Organization2 aLastName1 25 aLastName2 25 LastName3 25 aLastName4 25" + System.lineSeparator() +
                "Organization5 OOO LasName5 Name5 Patronymic5 50 Organization1 LastName3 100";

        ByteArrayInputStream in = new ByteArrayInputStream((data + System.lineSeparator() + System.lineSeparator()).getBytes());
        System.setIn(in);

        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baOut);
        System.setOut(out);

        Runner.main(param);

        System.setOut(sysOut);
        System.setIn(sysIn);

        System.out.println(baOut.toString());
        assertEquals("runnerTest", expected, baOut.toString());
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.util.HashMap;
import java.util.Map;

public class Util {
    public static String getRichestShareholderInfo(OrganizationsList orgs) {
        Map<String, Double> map = new HashMap<>();
        for (Map.Entry<String, Organization> org : orgs.getOrgs().entrySet()) {
            for (Shareholder sh : org.getValue().getShareholders()) {
                if (map.containsKey(sh.getLastName())) {
                    map.put(sh.getLastName(), map.get(sh.getLastName()) +
                            sh.getSharesPercentage() * org.getValue().getAuthorizedCapital());
                } else {
                    map.put(sh.getLastName(), sh.getSharesPercentage() * org.getValue().getAuthorizedCapital());
                }
            }
        }
        String key = "";
        double value = 0;
        for (Map.Entry<String, Double> sh : map.entrySet()) {
            if (sh.getValue() > value) {
                value = sh.getValue();
                key = sh.getKey();
            }
        }
//        System.out.println("The richest shareholder is " + key + " with capital equals " + value + " a.u.");
        return key;
    }

    public static String getBiggestOrgInfo(OrganizationsList orgs) {
        Map<String, Integer> map = new HashMap<>();
        for (Map.Entry<String, Organization> org : orgs.getOrgs().entrySet()) {
            Organization parent = org.getValue().getParent();
            if (parent != null) {
                if (map.containsKey(parent.getName())) {
                    map.put(parent.getName(), map.get(parent.getName()) + 1);
                } else {
                    map.put(parent.getName(), 1);
                }
            }
        }
        String key = "";
        int value = 0;
        for (Map.Entry<String, Integer> ob : map.entrySet()) {
            if (ob.getValue() > value) {
                value = ob.getValue();
                key = ob.getKey();
            }
        }
//        System.out.println("The biggest organization is " + key + " with the number of subsidiaries equals " + value + ".");
        return key;
    }
}

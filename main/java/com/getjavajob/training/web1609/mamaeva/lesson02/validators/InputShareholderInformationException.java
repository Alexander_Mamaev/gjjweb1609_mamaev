package com.getjavajob.training.web1609.mamaeva.lesson02.validators;

public class InputShareholderInformationException extends RuntimeException {
    public InputShareholderInformationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputShareholderInformationException(String message) {
        super(message);
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson02.validators;

public class LegalEntityException extends RuntimeException {
    public LegalEntityException(String msg, Exception cause) {
        super(msg, cause);
    }
}

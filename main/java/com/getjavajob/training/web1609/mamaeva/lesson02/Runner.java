package com.getjavajob.training.web1609.mamaeva.lesson02;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.OrganizationsList;

import java.io.File;

import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Serializator.deserialization;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Serializator.serialization;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getBiggestOrgInfo;
import static com.getjavajob.training.web1609.mamaeva.lesson02.organization.Util.getRichestShareholderInfo;
import static com.getjavajob.training.web1609.mamaeva.util.ConsoleReader.readString;

public class Runner {
    public static void main(String[] args) {
        File serial = new File(Runner.class.getResource("Organizations.dat").getPath());

        OrganizationsList orgs = new OrganizationsList();

        if (args != null && args.length != 0 && args[0] != null) {
            if (serial.exists()) {
                orgs = deserialization(serial);
            }
            orgs.readFrom(new File(args[0]));
        } else {
            orgs = new OrganizationsList();
            String line;
            System.out.println("Please enter the organization information (enter the empty line to input)");
            while ((line = readString()).length() > 0) {
                orgs.readFrom(line);
            }
        }

        System.out.println(getRichestShareholderInfo(orgs));
        System.out.println(getBiggestOrgInfo(orgs));

        serialization(orgs, serial);
    }
}

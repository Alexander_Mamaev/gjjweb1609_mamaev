package com.getjavajob.training.web1609.mamaeva.lesson02.validators;

public class AuthorizedCapitalException extends RuntimeException {
    public AuthorizedCapitalException(String msg, Exception cause) {
        super(msg, cause);
    }

    public AuthorizedCapitalException(String message) {
        super(message);
    }
}

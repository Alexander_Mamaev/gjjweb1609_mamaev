package com.getjavajob.training.web1609.mamaeva.lesson02;

import com.getjavajob.training.web1609.mamaeva.lesson02.testFiles.ExceptionsTest;
import com.getjavajob.training.web1609.mamaeva.lesson02.testFiles.IOTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ExceptionsTest.class, IOTest.class})
public class RunnerTest {
}
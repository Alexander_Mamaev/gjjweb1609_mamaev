package com.getjavajob.training.web1609.mamaeva.lesson02.organization;

import java.io.Serializable;

import static com.getjavajob.training.web1609.mamaeva.lesson02.validators.Validators.nameValidator;
import static com.getjavajob.training.web1609.mamaeva.lesson02.validators.Validators.percentageValidator;

/**
 * describes information about shareholders
 */
public class Shareholder implements Serializable {

    private static final long serialVersionUID = 20161002;

    private String lastName;
    private double sharesPercentage;

    public Shareholder() {
    }

    public Shareholder(String lastName, String sharesPercentage) {
        this.lastName = nameValidator(lastName);
        this.sharesPercentage = percentageValidator(sharesPercentage);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = nameValidator(lastName);
    }

    public double getSharesPercentage() {
        return sharesPercentage;
    }

    public void setSharesPercentage(String sharesPercentage) {
        this.sharesPercentage = percentageValidator(sharesPercentage);
    }

    @Override
    public String toString() {
        return "Shareholder{" +
                "lastName='" + lastName + '\'' +
                ", sharesPercentage=" + sharesPercentage +
                '}';
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson02.validators;

import com.getjavajob.training.web1609.mamaeva.lesson02.organization.LegalEntity;
import com.getjavajob.training.web1609.mamaeva.lesson02.organization.Shareholder;

import java.util.Collection;

public class Validators {
    public static String[] inputValidator(String[] line) {
        if (line.length % 2 == 0) {
            throw new InputShareholderInformationException("Incorrect size of the input line");
        }
        return line;
    }

    public static String nameValidator(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name validation failed");
        }
        return name;
    }

    public static LegalEntity entityValidator(String entity) {
        try {
            return LegalEntity.valueOf(entity);
        } catch (IllegalArgumentException e) {
            throw new LegalEntityException("Legal entity validation failed", e);
        }
    }

    public static double authorizedCapitalValidator(String number) {
        double capital;
        try {
            capital = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            throw new AuthorizedCapitalException("Incorrect value of authorized capital", e);
        }
        if (capital < 0) {
            throw new AuthorizedCapitalException("Authorized capital validation failed");
        }
        return capital;
    }

    public static double percentageValidator(String percentage) {
        double n;
        try {
            n = Double.parseDouble(percentage);
        } catch (NumberFormatException e) {
            throw new ShareholderInformationException("Not a number in shares percentage input", e);
        }
        if (n < 0 || n > 100) {
            throw new ShareholderInformationException("Incorrect input of percentage");
        }
        return n;
    }

    public static boolean collectivePercentageValidator(Collection<Shareholder> col) {
        double d = 0;
        for (Shareholder sh : col) {
            d += sh.getSharesPercentage();
        }
        return d <= 100;
    }
}

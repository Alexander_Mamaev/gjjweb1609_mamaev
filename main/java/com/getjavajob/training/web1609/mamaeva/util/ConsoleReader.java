package com.getjavajob.training.web1609.mamaeva.util;

import java.util.Scanner;

public class ConsoleReader {
    private static Scanner scanner = new Scanner(System.in);

    public static int readInt() {
        return readInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static double readDouble() {
        return readDouble(Double.MIN_VALUE, Double.MAX_VALUE);
    }

    public static String readString() {
        return scanner.nextLine();
    }

    public static int readInt(int min, int max) {
        int number;
        while (true) {
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number < min || number > max) {
                    System.out.println("Incorrect number! Please try again.");
                } else {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("This is not a number! Please try again.");
            }
        }
        return number;
    }

    public static double readDouble(double min, double max) {
        double number;
        while (true) {
            try {
                number = Double.parseDouble(scanner.nextLine());
                if (number < min || number > max) {
                    System.out.println("Incorrect number! Please try again.");
                } else {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("This is not a number! Please try again.");
            }
        }
        return number;
    }
}

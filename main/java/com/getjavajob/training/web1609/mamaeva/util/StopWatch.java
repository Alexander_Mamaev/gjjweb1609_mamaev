package com.getjavajob.training.web1609.mamaeva.util;

public class StopWatch {
    private long startTime;

    public long getStartTime() {
        return this.startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long start() {
        long currentTime = System.currentTimeMillis();
        setStartTime(currentTime);
        return currentTime;
    }

    public long getElapsedTime() {
        return System.currentTimeMillis() - getStartTime();
    }
}

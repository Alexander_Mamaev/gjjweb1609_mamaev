package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

public class IdValidator implements Validator {
    public void validate(Product product) {
        String id = product.getId();
        if (id == null || id.length() <= 0 || !id.codePoints().allMatch(Character::isUpperCase)) {
            throw new ValidationException("Id is not valid!");
        }

    }
}

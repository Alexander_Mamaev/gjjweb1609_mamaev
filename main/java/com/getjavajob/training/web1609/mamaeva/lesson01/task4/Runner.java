package com.getjavajob.training.web1609.mamaeva.lesson01.task4;

import java.lang.reflect.Field;

public class Runner {
    public static void main(String[] args) {
        Field[] fields = Product.class.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Description.class)) {
                System.out.println(f.getName() + " - " + f.getAnnotation(Description.class).str());
            }
        }
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson01.task3;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class RunnerTest {
    @Test
    public void main() throws Exception {
        InputStream sysIn = System.in;
        PrintStream sysOut = System.out;

        String input = "AMERICA\r\nEUROPE\r\nASIA\r\nAFRICA\r\nAUSTRALIA\r\nANTARCTICA\r\n";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baOut);
        System.setOut(out);

        Runner.main(new String[]{});
        Runner.main(new String[]{});
        Runner.main(new String[]{});
        Runner.main(new String[]{});
        Runner.main(new String[]{});
        Runner.main(new String[]{});

        System.setOut(sysOut);
        System.setIn(sysIn);

        String expected = "AMERICA: area=43, population=954, numberOfCountries=35\r\n" +
                "EUROPE: area=10, population=740, numberOfCountries=50\r\n" +
                "ASIA: area=45, population=4164, numberOfCountries=54\r\n" +
                "AFRICA: area=30, population=1100, numberOfCountries=55\r\n" +
                "AUSTRALIA: area=7, population=24, numberOfCountries=1\r\n" +
                "ANTARCTICA: area=52, population=0, numberOfCountries=0\r\n";

        assertEquals("WordParts Runner Test", expected, baOut.toString());
        System.out.println(baOut.toString());
    }
}
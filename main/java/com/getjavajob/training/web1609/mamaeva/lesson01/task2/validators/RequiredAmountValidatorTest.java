package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class RequiredAmountValidatorTest {
    private static Product product;
    private Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        product.setAvailableAmount(10);
        validator = new RequiredAmountValidator();
    }

    @Test
    public void validateTest() {
        product.setRequiredAmount(5);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest2() {
        product.setRequiredAmount(0);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest3() {
        product.setRequiredAmount(-123);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest4() {
        product.setRequiredAmount(11);
        validator.validate(product);
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.ClosedRegularFigure2D;

public interface RegularFigure3D extends ClosedRegularFigure2D {
    /**
     * @return the volume of figure
     */
    double volume();

    /**
     * @return the area of the cross section of figure
     */
    double crossSectionArea(double crossSectionHeight);

    /**
     * @return the area of all surface of figure
     */
    double surfaceArea();
}

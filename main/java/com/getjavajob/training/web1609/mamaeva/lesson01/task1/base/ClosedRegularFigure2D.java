package com.getjavajob.training.web1609.mamaeva.lesson01.task1.base;

public interface ClosedRegularFigure2D extends Cloneable {
    /**
     * @return the area of figure
     */
    double baseArea();

    /**
     * @return the perimeter of figure
     */
    double perimeter();

    /**
     * @return the radius of inscribed circle
     */
    double getRadius();

    ClosedRegularFigure2D clone();
}

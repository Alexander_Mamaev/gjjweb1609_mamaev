package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

import java.util.regex.Pattern;

public class PhoneNumberValidator implements Validator {
    public void validate(Product product) {
        if (!Pattern.compile("^\\+7{1}9{1}\\d{9}$").matcher(product.getPhoneNumber()).matches()) {
            throw new ValidationException("Phone number is not valid!");
        }
    }
}

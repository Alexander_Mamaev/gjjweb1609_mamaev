package com.getjavajob.training.web1609.mamaeva.lesson01.task1.base;

public class RegularPolyhedron implements ClosedRegularFigure2D {
    private int angleNumber;
    private double edgeLength;

    public RegularPolyhedron(int angleNumber, double edgeLength) {
        this.angleNumber = angleNumber;
        this.edgeLength = edgeLength;
    }

    @Override
    public double baseArea() {
        return angleNumber / 4. * edgeLength * edgeLength / Math.tan(Math.PI / angleNumber);
    }

    @Override
    public double perimeter() {
        return angleNumber * edgeLength;
    }

    // return the radius of inscribed circle
    @Override
    public double getRadius() {
        return 2 * baseArea() / perimeter();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegularPolyhedron that = (RegularPolyhedron) o;

        if (angleNumber != that.angleNumber) return false;
        return Double.compare(that.edgeLength, edgeLength) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = angleNumber;
        temp = Double.doubleToLongBits(edgeLength);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public RegularPolyhedron clone() {
        try {
            return (RegularPolyhedron) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}

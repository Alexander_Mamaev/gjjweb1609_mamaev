package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

public class PriceValidator implements Validator {
    public void validate(Product product) {
        if (product.getPrice() <= 0) {
            throw new ValidationException("Price is not valid!");
        }
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.ClosedRegularFigure2D;

public abstract class VolumeRegularFigure implements RegularFigure3D {
    private ClosedRegularFigure2D base;
    private double height;

    public VolumeRegularFigure(ClosedRegularFigure2D base, double height) {
        this.height = height;
        this.base = base;
    }

    @Override
    public double baseArea() {
        return base.baseArea();
    }

    @Override
    public double perimeter() {
        return base.perimeter();
    }

    @Override
    public double getRadius() {
        return base.getRadius();
    }

    public ClosedRegularFigure2D getBase() {
        return base;
    }

    public void setBase(ClosedRegularFigure2D base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "baseArea: " + baseArea() + System.lineSeparator() +
                "surfaceArea: " + surfaceArea() + System.lineSeparator() +
                "volume: " + volume();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    public boolean figureEquals(VolumeRegularFigure figure) {
        if (Double.compare(figure.height, height) != 0) return false;
        return base != null ? base.equals(figure.base) : figure.base == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = base != null ? base.hashCode() : 0;
        temp = Double.doubleToLongBits(height);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public VolumeRegularFigure clone() {
        try {
            ClosedRegularFigure2D newBase = base.clone();
            VolumeRegularFigure figure = (VolumeRegularFigure) super.clone();
            figure.setBase(newBase);
            return figure;
        } catch (CloneNotSupportedException e) {
            //NOP
            return null;
        }
    }
}

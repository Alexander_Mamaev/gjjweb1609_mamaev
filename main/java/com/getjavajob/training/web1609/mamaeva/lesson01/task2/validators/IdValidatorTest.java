package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class IdValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new IdValidator();
    }

    @Test
    public void validateTest() {
        product.setId("ИП");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest2() {
        product.setId(null);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest3() {
        product.setId("Ип");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest4() {
        product.setId("");
        validator.validate(product);
    }
}
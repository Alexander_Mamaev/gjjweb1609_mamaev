package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

import java.util.regex.Pattern;

public class EmailValidator implements Validator {
    public void validate(Product product) {
        if (!Pattern.compile("^[^@]+@[^@.]+\\.[^@]+$").matcher(product.getEmail()).matches()) {
            throw new ValidationException("Email is not valid!");
        }
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

public class RequiredAmountValidator implements Validator {
    public void validate(Product product) {
        if (product.getRequiredAmount() <= 0 || product.getRequiredAmount() > product.getAvailableAmount()) {
            throw new ValidationException("Required amount is not valid!");
        }
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson01.task4;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class RunnerTest {
    @Test
    public void main() throws Exception {
        PrintStream sysOut = System.out;

        ByteArrayOutputStream baOut = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baOut);
        System.setOut(out);

        Runner.main(new String[]{});

        System.setOut(sysOut);

        String expected = "name - name of the product\r\n" +
                "requiredAmount - required amount of the product\r\n" +
                "availableAmount - available amount of the product in the warehouse\r\n" +
                "price - price of the product\r\n" +
                "discount - discount value\r\n" +
                "id - identificator of the client\r\n" +
                "email - e-mail of the client\r\n" +
                "phoneNumber - phone number of the client\r\n" +
                "deliveryDate - date of the delivery (dd.MM.yyyy)\r\n";

        assertEquals("Description Runner Test", expected, baOut.toString());
        System.out.println(baOut);
    }

}
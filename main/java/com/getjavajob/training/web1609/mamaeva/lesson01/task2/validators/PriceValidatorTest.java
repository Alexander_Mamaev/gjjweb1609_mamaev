package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class PriceValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new PriceValidator();
    }

    @Test
    public void validateTest() {
        product.setPrice(12);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest2() {
        product.setPrice(0);
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest3() {
        product.setPrice(-56);
        validator.validate(product);
    }
}
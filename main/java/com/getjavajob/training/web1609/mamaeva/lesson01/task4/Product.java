package com.getjavajob.training.web1609.mamaeva.lesson01.task4;

public class Product {
    @Description(str = "name of the product")
    private String name;
    @Description(str = "required amount of the product")
    private int requiredAmount;
    @Description(str = "available amount of the product in the warehouse")
    private int availableAmount;
    @Description(str = "price of the product")
    private int price;
    @Description(str = "discount value")
    private int discount;
    @Description(str = "identificator of the client")
    private String id;
    @Description(str = "e-mail of the client")
    private String email;
    @Description(str = "phone number of the client")
    private String phoneNumber;
    @Description(str = "date of the delivery (dd.MM.yyyy)")
    private String deliveryDate;
}

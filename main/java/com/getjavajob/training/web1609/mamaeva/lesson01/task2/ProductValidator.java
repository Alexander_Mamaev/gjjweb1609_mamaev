package com.getjavajob.training.web1609.mamaeva.lesson01.task2;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators.Validator;

public class ProductValidator {
    private Validator[] validators;

    public ProductValidator(Validator[] validationType) {
        this.validators = validationType;
    }

    public Validator[] getValidators() {
        return validators;
    }

    public void setValidators(Validator[] validators) {
        this.validators = validators;
    }

    public void validate(Product product) {
        for (Validator v : validators) {
            try {
                v.validate(product);
            } catch (ValidationException e) {
                throw new ValidationException("Product is not valid!", e);
            }
        }
    }
}

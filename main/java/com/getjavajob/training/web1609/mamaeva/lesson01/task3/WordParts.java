package com.getjavajob.training.web1609.mamaeva.lesson01.task3;

enum WordParts {
    AMERICA(43, 954, 35), EUROPE(10, 740, 50), ASIA(45, 4_164, 54), AFRICA(30, 1_100, 55), AUSTRALIA(7, 24, 1), ANTARCTICA(52, 0, 0);

    private int area; // millions km^2
    private int population; // millions
    private int numberOfCountries;

    WordParts(int area, int population, int numberOfCountries) {
        this.area = area;
        this.population = population;
        this.numberOfCountries = numberOfCountries;
    }

    @Override
    public String toString() {
        return this.name() + ": " +
                "area=" + area +
                ", population=" + population +
                ", numberOfCountries=" + numberOfCountries;
    }
}

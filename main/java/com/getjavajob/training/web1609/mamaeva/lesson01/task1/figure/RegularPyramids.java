package com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.ClosedRegularFigure2D;

public abstract class RegularPyramids extends VolumeRegularFigure {
    public RegularPyramids(ClosedRegularFigure2D base, double height) {
        super(base, height);
    }

    @Override
    public double volume() {
        return baseArea() * getHeight() / 3;
    }

    @Override
    public double surfaceArea() {
        return perimeter() * guidingLineLength() / 2 + baseArea();
    }

    private double guidingLineLength() { // aka apophthegm
        return Math.pow(Math.pow(getRadius(), 2) + Math.pow(getHeight(), 2), 0.5);
    }

    @Override
    public double crossSectionArea(double crossSectionHeight) {
        return baseArea() * (Math.pow(crossSectionHeight, 2) / Math.pow(getHeight(), 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof RegularPyramids)) return false;
        RegularPyramids figure = (RegularPyramids) o;
        return this.figureEquals(figure);
    }
}

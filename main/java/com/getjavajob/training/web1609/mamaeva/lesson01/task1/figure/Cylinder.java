package com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.Circle;

public class Cylinder extends RegularPrisms {
    public Cylinder(double radius, double height) {
        super(new Circle(radius), height);
    }
}

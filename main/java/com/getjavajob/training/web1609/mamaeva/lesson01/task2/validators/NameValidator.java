package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

public class NameValidator implements Validator {
    public void validate(Product product) {
        if (product.getName() == null || product.getName().length() <= 0) {
            throw new ValidationException("Name is not valid!");
        }
    }
}

package com.getjavajob.training.web1609.mamaeva.lesson01.task2;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators.*;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class ValidatorTest {
    private Product product;

    @Before
    public void setUp() throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Calendar date = new GregorianCalendar();
        date.add(Calendar.DAY_OF_MONTH, 1);

        product = new Product();
        product.setName("name");
        product.setRequiredAmount(5);
        product.setAvailableAmount(10);
        product.setPrice(5);
        product.setDiscount(2);
        product.setId("IP");
        product.setEmail("mail@mail.ru");
        product.setPhoneNumber("+79221234567");
        product.setDeliveryDate(df.format(date.getTime()));
    }

    @Test
    public void validateTest1() {
        Validator[] validationType = new Validator[]{new NameValidator(),
                new RequiredAmountValidator(), new PriceValidator(), new DiscountValidator(),
                new IdValidator(), new EmailValidator(), new PhoneNumberValidator(), new DeliveryDateValidator()};

        ProductValidator productValidator = new ProductValidator(validationType);
        productValidator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest2() {
        product.setDiscount(5);
        product.setEmail("@mail@mail.ru");

        Validator[] validationType = new Validator[]{new NameValidator(),
                new RequiredAmountValidator(), new PriceValidator(), new DiscountValidator(),
                new IdValidator(), new EmailValidator(), new PhoneNumberValidator(), new DeliveryDateValidator()};

        ProductValidator productValidator = new ProductValidator(validationType);
        productValidator.validate(product);
    }

    @Test
    public void validateTest3() {
        product.setDiscount(5);
        product.setEmail("@mail@mail.ru");

        Validator[] validationType = new Validator[]{new NameValidator(),
                new RequiredAmountValidator(), new PriceValidator(), new DeliveryDateValidator()};

        ProductValidator productValidator = new ProductValidator(validationType);
        productValidator.validate(product);
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson01.task3;

import static com.getjavajob.training.web1609.mamaeva.util.ConsoleReader.readString;

public class Runner {
    public static void main(String[] args) {
        String partOfWord = readString();
        System.out.println(WordParts.valueOf(partOfWord));
    }
}

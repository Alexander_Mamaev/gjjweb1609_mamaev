package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class NameValidatorTest {
    private static Product product;
    private static Validator validator;

    @Before
    public void setUp() {
        product = new Product();
        validator = new NameValidator();
    }

    @Test
    public void validateTest() {
        product.setName("name");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest2() {
        product.setName("");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest3() {
        product.setName(null);
        validator.validate(product);
    }
}
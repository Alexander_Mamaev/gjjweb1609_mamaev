package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;
import org.junit.Before;
import org.junit.Test;

public class EmailValidatorTest {
    private static Validator validator;
    private static Product product;

    @Before
    public void setUp() {
        validator = new EmailValidator();
        product = new Product();
    }

    @Test
    public void validateTest() {
        product.setEmail("mail@mail.ru");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validaTest2() {
        product.setEmail("@mail@mail.ru");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest3() {
        product.setEmail("mail@mailru");
        validator.validate(product);
    }

    @Test (expected = ValidationException.class)
    public void validateTest4() {
        product.setEmail("mailmail.ru");
        validator.validate(product);
    }

    @Test
    public void validateTest5() {
        product.setEmail("mail@mail.ru.com");
        validator.validate(product);
    }
}
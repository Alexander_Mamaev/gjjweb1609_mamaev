package com.getjavajob.training.web1609.mamaeva.lesson01.task1;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.Circle;
import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.RegularPolyhedron;
import com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FigureTest {
    @Test
    public void cloneTest() throws Exception {
        VolumeRegularFigure figure1 = new Cone(5, 6);
        VolumeRegularFigure figure2 = figure1.clone();
        assertEquals("cloneTest1", false, figure1 == figure2);
        assertEquals("cloneTest2", true, figure1.equals(figure2));
        assertEquals("cloneTest3", false, figure1.getBase() == figure2.getBase());
        assertEquals("cloneTest4", true, figure1.getBase().equals(figure2.getBase()));
    }

    @Test
    public void equalsTest() {
        VolumeRegularFigure figure0 = new VolumeRegularFigure(new RegularPolyhedron(6, 3), 10) {
            @Override
            public double volume() {
                return 0;
            }

            @Override
            public double crossSectionArea(double crossSectionHeight) {
                return 0;
            }

            @Override
            public double surfaceArea() {
                return 0;
            }
        };
        VolumeRegularFigure figure1 = new RegularPyramids(new RegularPolyhedron(6, 3), 10) {
        };
        VolumeRegularFigure figure2 = new RegularPrisms(new Circle(5), 10) {
        };
        VolumeRegularFigure figure3 = new Prism(6, 3, 10);
        VolumeRegularFigure figure4 = new Pyramid(6, 3, 10);
        VolumeRegularFigure figure5 = new Cylinder(5, 10);
        assertEquals("equalsTest0.1", false, figure0.equals(figure1));
        assertEquals("equalsTest0.2", false, figure1.equals(figure0));
        assertEquals("equalsTest1.1", false, figure1.equals(figure2));
        assertEquals("equalsTest1.2", false, figure2.equals(figure1));
        assertEquals("equalsTest2.1", true, figure1.equals(figure4));
        assertEquals("equalsTest2.2", true, figure4.equals(figure1));
        assertEquals("equalsTest3.1", false, figure3.equals(figure4));
        assertEquals("equalsTest3.2", false, figure4.equals(figure3));
        assertEquals("equalsTest4.1", true, figure2.equals(figure5));
        assertEquals("equalsTest4.2", true, figure5.equals(figure2));

        assertEquals("equalsTest5.1", true, figure0.equals(figure0));
        assertEquals("equalsTest5.2", true, figure1.equals(figure1));
        assertEquals("equalsTest5.3", true, figure2.equals(figure2));
        assertEquals("equalsTest5.4", true, figure3.equals(figure3));
        assertEquals("equalsTest5.5", true, figure4.equals(figure4));

        assertEquals("equalsTest6.1", false, figure5.equals(null));
        assertEquals("equalsTest6.2", false, figure1.equals(null));
    }
}
package com.getjavajob.training.web1609.mamaeva.lesson01.task2.validators;

import com.getjavajob.training.web1609.mamaeva.lesson01.task2.Product;
import com.getjavajob.training.web1609.mamaeva.lesson01.task2.ValidationException;

public class DiscountValidator implements Validator {
    private static final double DISCOUNT_LIMIT = 0.2;

    public  void validate(Product product) {
        if (product.getDiscount() <= 0 ||
                product.getDiscount() >= product.getRequiredAmount() * product.getPrice() * DISCOUNT_LIMIT) {
            throw new ValidationException("Discount limit is not valid!");
        }
    }
}

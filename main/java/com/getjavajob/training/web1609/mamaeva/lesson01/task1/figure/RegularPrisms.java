package com.getjavajob.training.web1609.mamaeva.lesson01.task1.figure;

import com.getjavajob.training.web1609.mamaeva.lesson01.task1.base.ClosedRegularFigure2D;

public abstract class RegularPrisms extends VolumeRegularFigure {
    public RegularPrisms(ClosedRegularFigure2D base, double height) {
        super(base, height);
    }

    @Override
    public double volume() {
        return baseArea() * getHeight();
    }

    @Override
    public double crossSectionArea(double crossSectionHeight) {
        return baseArea();
    }

    @Override
    public double surfaceArea() {
        return perimeter() * getHeight() + baseArea();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof RegularPrisms)) return false;
        RegularPrisms figure = (RegularPrisms) o;
        return this.figureEquals(figure);
    }
}
